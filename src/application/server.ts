import {MONGODB_URI, PORT} from './config/config'
import {MongoHelper} from "../infrastructure/driver_adapters/helpers/mongo-helper";

MongoHelper.connect(MONGODB_URI)
    .then(async () => {
        console.log('Connected DB')
        const app = (await import('./config/app-config')).default
        app.listen(PORT, () => console.log(`Server an running on port: ${PORT}`))
    }).catch(error => console.log(error))
import {adaptMiddleware} from "../config/middleware-adapter";
import {makeAuthMiddleware} from "../../infrastructure/entry_points/api/factories/auth-middleware-factory";

export const auth = adaptMiddleware(makeAuthMiddleware())
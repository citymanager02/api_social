import {IMiddleware} from "../../infrastructure/entry_points/gateways/middleware";
import {HttpResponse, ok, serverError} from "../../infrastructure/helpers/http";
import {ILoadAccountByTokenService} from "../../domain/use_cases/gateways/auth/load-account-by-token-service";

/**
 * Esta clase se encarga de recibir el token en la peticion,
 * para luego pasarle la instancia a la funcion del middleware adapt
 * quien se encarga de validarlo y asi poder acceder a los
 * endpoints protegidos con la funcion adaptMiddleware
 *
 */
export class AuthMiddleware implements IMiddleware {
    constructor(private readonly loadAccountByTokenService: ILoadAccountByTokenService) {
    }

    async handle(httpRequest: any): Promise<HttpResponse> {
        try {
            const {accessToken} = httpRequest

            if (accessToken) {
                const account = await this.loadAccountByTokenService.loadByTokenService(accessToken)
                if (account) {
                    return ok(account)
                }
            }
        } catch (e) {
           return serverError(e)
        }
    }
}
import {Router} from "express";
import {adaptRoute} from "../../application/config/express-router-adapter";
import {makeBaseControllerFactory} from "../../infrastructure/entry_points/api/factories/base-controller-factory";
import {
    ADD_POSTS,
    ADD_POSTS_COMMENTS,
    ADD_POSTS_LIKES, DELETE_POSTS, DELETE_POSTS_COMMENTS, DELETE_POSTS_LIKES,
    LOAD_POSTS,
    LOAD_POSTS_BY_ID
} from "../../infrastructure/helpers/constants";

export default (router: Router): void => {
    router.get('/posts', adaptRoute(makeBaseControllerFactory(LOAD_POSTS)))
    router.get('/posts/:id', adaptRoute(makeBaseControllerFactory(LOAD_POSTS_BY_ID)))
    router.post('/posts', adaptRoute(makeBaseControllerFactory(ADD_POSTS)))
    router.delete('/posts/delete/:id', adaptRoute(makeBaseControllerFactory(DELETE_POSTS)))
    router.put('/posts/:id/likes', adaptRoute(makeBaseControllerFactory(ADD_POSTS_LIKES)))
    router.put('/posts/:postId/delete/likes/:likeId', adaptRoute(makeBaseControllerFactory(DELETE_POSTS_LIKES)))
    router.put('/posts/:id/comment', adaptRoute(makeBaseControllerFactory(ADD_POSTS_COMMENTS)))
    router.put('/posts/:postId/delete/comment/:commentId', adaptRoute(makeBaseControllerFactory(DELETE_POSTS_COMMENTS)))
}
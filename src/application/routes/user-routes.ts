import {Router} from "express";
import {adaptRoute} from "../config/express-router-adapter";
import {auth} from "../middlewares/auth";
import {makeBaseControllerFactory} from "../../infrastructure/entry_points/api/factories/base-controller-factory";
import {ADD_USERS, LOAD_USER_BY_ID} from "../../infrastructure/helpers/constants";

export default (router: Router): void => {
    router.post('/users', adaptRoute(makeBaseControllerFactory(ADD_USERS)))
    router.get('/users/:id', adaptRoute(makeBaseControllerFactory(LOAD_USER_BY_ID)))
}
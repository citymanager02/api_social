import {Router} from "express";
import {adaptRoute} from "../../application/config/express-router-adapter";
import {makeBaseControllerFactory} from "../../infrastructure/entry_points/api/factories/base-controller-factory";
import {LOGIN} from "../../infrastructure/helpers/constants";

export default (router: Router): void => {
    router.post('/login', adaptRoute(makeBaseControllerFactory(LOGIN)))
}
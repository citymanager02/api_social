import {Router} from "express";
import {adaptRoute} from "../../application/config/express-router-adapter";
import {auth} from "../../application/middlewares/auth";
import {makeBaseControllerFactory} from "../../infrastructure/entry_points/api/factories/base-controller-factory";
import {
    ADD_PROFILES,
    ADD_PROFILES_EDUCATIONS,
    ADD_PROFILES_EXPERIENCES, DELETE_PROFILE, DELETE_PROFILE_EDUCATION, DELETE_PROFILE_EXPERIENCES,
    LOAD_PROFILE_BY_ID
} from "../../infrastructure/helpers/constants";

export default (router: Router): void => {
    router.get('/profiles/:id', adaptRoute(makeBaseControllerFactory(LOAD_PROFILE_BY_ID)))
    router.post('/profiles', adaptRoute(makeBaseControllerFactory(ADD_PROFILES)))
    router.delete('/profiles/:userId', adaptRoute(makeBaseControllerFactory(DELETE_PROFILE)))
    router.put('/profiles/:id/experience', adaptRoute(makeBaseControllerFactory(ADD_PROFILES_EXPERIENCES)))
    router.put('/profiles/:profileId/delete/experience/:experienceId', adaptRoute(makeBaseControllerFactory(DELETE_PROFILE_EXPERIENCES)))
    router.put('/profiles/:id/education', adaptRoute(makeBaseControllerFactory(ADD_PROFILES_EDUCATIONS)))
    router.put('/profiles/:profileId/delete/education/:educationId', adaptRoute(makeBaseControllerFactory(DELETE_PROFILE_EDUCATION)))
}
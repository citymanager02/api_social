import {Request, Response} from 'express'
import {IController} from "@/infrastructure/entry_points/gateways/controller";
import {HttpRequest} from "@/infrastructure/helpers/http";

/**
 * Este helper nos conecta todos los controladores[capa de infraestructura] con cualquier framework de NodeJs,
 * en este caso(Express) a traves del gateway {@code IController}, el gateway {@code IController} se recibe
 * como argumento en el helper {@code adaptRoute}, esto garantiza que la implementación del {@code Request}
 * y el {@code Response} sea la adecuada para el procesamiento de las peticiones que llega desde las rutas.
 * @version 1.0
 * @author John Piedrahita
 *
 * @param controller
 */
export const adaptRoute = (controller: IController) => {
    return async (req: Request, res: Response) => {
        const httpRequest: HttpRequest = {
            body: req.body,
            params: req.params
        }

        const httpResponse = await controller.handle(httpRequest)

        if (httpResponse.statusCode >= 200 && httpResponse.statusCode <= 299) {
            res.status(httpResponse.statusCode).json(httpResponse.body)
        } else {
            res.status(httpResponse.statusCode).json({
                error: httpResponse.body.message
            })
        }
    }
}
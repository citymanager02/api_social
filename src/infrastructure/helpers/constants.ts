// =============== USERS ===================== //
export const LOAD_USER_BY_ID = "LOAD_USER_BY_ID"
export const ADD_USERS = "ADD_USERS"

// =============== PROFILES ================== //
export const ADD_PROFILES = "ADD_PROFILES"
export const ADD_PROFILES_EXPERIENCES = "ADD_PROFILES_EXPERIENCES"
export const ADD_PROFILES_EDUCATIONS = "ADD_PROFILES_EDUCATIONS"
export const LOAD_PROFILE_BY_ID = "LOAD_PROFILE_BY_ID"
export const DELETE_PROFILE = "DELETE_PROFILE"
export const DELETE_PROFILE_EXPERIENCES = "DELETE_PROFILE_EXPERIENCES"
export const DELETE_PROFILE_EDUCATION = "DELETE_PROFILE_EDUCATION"

// =============== POSTS ===================== //
export const ADD_POSTS = "ADD_POSTS"
export const ADD_POSTS_COMMENTS = "ADD_POSTS_COMMENTS"
export const ADD_POSTS_LIKES = "ADD_POSTS_LIKES"
export const LOAD_POSTS_BY_ID = "LOAD_POSTS_BY_ID"
export const LOAD_POSTS = "LOAD_POSTS"
export const DELETE_POSTS = "DELETE_POSTS"
export const DELETE_POSTS_COMMENTS = "DELETE_POSTS_COMMENTS"
export const DELETE_POSTS_LIKES = "DELETE_POSTS_LIKES"

// =============== AUTH ====================== //
export const LOGIN = "LOGIN"



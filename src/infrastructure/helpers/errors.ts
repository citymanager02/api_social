/**
 * Clases que nos provee los mensajes de error para dar manejo de forma más efectiva
 * @version 1.0
 * @author John Piedrahita
 */

/**
 * Clase que nos provee el mensaje de error cuando hay una respuesta con un
 * statusCode 500 o error de servidor
 */
export class ServerError extends Error {
    constructor(stack?: string) {
        super('Error en el servidor');
        this.name = 'ServerError'
        this.stack = stack
    }
}

/**
 * Clase que nos provee el mensaje de error cuando un usuario no esta
 * autorizado a realizar un proceso en el sistema
 */
export class UnauthorizedError extends Error {
    constructor() {
        super('Unauthorized Error');
        this.name = 'UnauthorizedError'
    }
}

/**
 * Clase que nos provee el mensaje de error cuando el email de un usuario
 * ya existe en la base de datos
 */
export class EmailInUseError extends Error {
    constructor() {
        super('El email ya esta en uso');
        this.name = 'EmailInUseError'
    }
}

/**
 * Helper que nos provee las validaciones para los campos vacíos y
 * los email con un formato no valido.
 * @version 1.0
 * @author John Piedrahita
 */

const regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/

/**
 * Esta función valida que los campos no lleguen vacíos y que el email
 * tenga un formato válido, si existen errores se devuelven en un objeto
 * para su procesamiento.
 * @param data
 */
export const fieldsValidation = (data: any) => {
    let errors = {}
    for (const key in data) {
        if (isEmpty(data[key])) {
            errors[key] = `${key} fields is required`
        } else if (key === 'email' && !regex.test(data[key])) {
            errors[key] = `${key} is invalid`
        }
    }
    return {errors, isValid: isEmpty(errors)}
}

// Validamos los campos vacíos
const isEmpty = (value: any) =>
    value === undefined ||
    value === null ||
    typeof value === 'object' && Object.keys(value).length === 0 ||
    typeof value === 'string' && value.trim().length === 0

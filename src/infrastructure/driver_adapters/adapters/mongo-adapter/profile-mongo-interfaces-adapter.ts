import {IAddEntityRepository} from "@/domain/models/gateways/add-entity-repository";
import {ProfileModel} from "@/domain/models/profile-model";
import {ILoadEntityByIdRepository} from "@/domain/models/gateways/load-entity-by-id-repository";
import {IDeleteProfileRepository} from "@/domain/models/gateways/profiles/delete-profile-repository";
import {ILoadEntityByFieldRepository} from "@/domain/models/gateways/load-entity-by-field-repository";
import {IAddEntitySubDocumentRepository} from "@/domain/models/gateways/add-entity-sub-document-repository";
import {IDeleteEntitySubDocumentRepository} from "@/domain/models/gateways/delete-entity-sub-document-repository";

export interface ProfileMongoInterfacesAdapter<T> extends IAddEntityRepository<ProfileModel>,
    ILoadEntityByFieldRepository<string>,
    ILoadEntityByIdRepository<ProfileModel>,
    IDeleteEntitySubDocumentRepository,
    IDeleteProfileRepository,
    IAddEntitySubDocumentRepository<T> {
}
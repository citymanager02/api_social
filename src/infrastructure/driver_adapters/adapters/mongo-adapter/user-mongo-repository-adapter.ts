import {UserModel} from "../../../../domain/models/user-model";
import {MongoHelper} from "../../helpers/mongo-helper";
import {ILoadAccountByTokenRepository} from "@/domain/models/gateways/auth/load-account-by-token-repository";
import {UserMongoInterfacesAdapter} from "@/infrastructure/driver_adapters/adapters/mongo-adapter/user-mongo-interfaces-adapter";
import {AddUserParams} from "@/domain/models/gateways/entity-types";
import {ILoadEntityByIdRepository} from "@/domain/models/gateways/load-entity-by-id-repository";

/**
 * Clase que provee la comunicación entre el driven-adapters(BD)[capa de infraestructura]
 * y los use-cases(servicios)[capa del dominio] para realizar los diferentes consultas en
 * la base de datos. Las capas se comunican por medio de gateways repositories.
 * @version 1.0
 * @author John Piedrahita
 */
export class UserMongoRepositoryAdapter implements UserMongoInterfacesAdapter {

    /**
     * Esta función hace una búsqueda de un documento en una colección o tabla
     * de forma dinámica de acuerdo al argumento que reciba y se puede
     * utilizar con cualquier base de datos.
     *
     * El primer argumento es el {@code valor} por el que se va a filtrar
     * El segundo argumento es el {@code campo} por el cual va a buscar en una tabla o colección
     * El tercer argumento es la {@code tabla} o {colección} donde se hace la búsqueda
     *
     * @param value
     *
     * @return un User Model de acuerdo al email que recibe como parámetro
     */
    async loadEntityByFieldRepository(value: string): Promise<string> {
        const user = await MongoHelper.loadCollectionByField(value, 'email', 'users')
        return user && MongoHelper.map(user)
    }

    /**
     * Esta función inserta un documento en la colección {users}, con la data
     * que recibe desde el servicio - caso de uso (capa del dominio)
     * @param data
     *
     * @return el User Model creado
     */
    async addEntityRepository(data: AddUserParams): Promise<UserModel> {
        return await MongoHelper.insertCollection(data, 'users')
    }

    /**
     * Esta función hace la verificación de que el token que se envía sea valido y
     * coincida con el que tiene el usuario en la DB
     * @param token
     *
     * @return un User Model de acuerdo al token
     */
    async loadByTokenRepository(token: string): Promise<ILoadAccountByTokenRepository.Result> {
        const accountCollection = await MongoHelper.getCollection('users')
        const account = await accountCollection.findOne({accessToken: token})
        return account && MongoHelper.map(account)
    }

    /**
     * Esta función nos actualiza el token cada vez que el usuario se autentique
     * y asi poder mantener la validez de este.
     * @param id
     * @param token
     */
    async updateAccessToken(id: string, token: string): Promise<void> {
        const accountCollection = await MongoHelper.getCollection('users')
        await accountCollection.updateOne({
            _id: id
        }, {
            $set: {
                accessToken: token
            }
        })
    }

    /**
     * Esta función hace la búsqueda de un documento en una colección
     * de mongoDB por su ObjectId.
     * @param id
     *
     * @return un User Model de acuerdo al Object Id
     */
    async loadEntityByIdRepository(id: string | number): Promise<ILoadEntityByIdRepository.Result> {
        return await MongoHelper.loadCollectionById(id, '_id', 'users')
    }
}
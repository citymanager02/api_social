import {ObjectId} from 'mongodb'
import {ProfileMongoInterfacesAdapter} from "@/infrastructure/driver_adapters/adapters/mongo-adapter/profile-mongo-interfaces-adapter";
import {AddProfileParams} from "@/domain/models/gateways/entity-types";
import {ILoadEntityByIdRepository} from "@/domain/models/gateways/load-entity-by-id-repository";
import {serverError} from "../../../../infrastructure/helpers/http";
import {MongoHelper} from "../../../../infrastructure/driver_adapters/helpers/mongo-helper";
import {ProfileModel} from "../../../../domain/models/profile-model";

/**
 * Clase que provee la comunicación entre el driven-adapters(BD)[capa de infraestructura]
 * y los use-cases(servicios)[capa del dominio] para realizar los diferentes consultas en
 * la base de datos. Las capas se comunican por medio de gateways repositories.
 * @version 1.0
 * @author John Piedrahita
 */
export class ProfileMongoRepositoryAdapter<T> implements ProfileMongoInterfacesAdapter<T> {

    /**
     * Esta función inserta un documento en la colección {profiles}, con la data
     * que recibe desde el servicio - caso de uso (capa del dominio)
     * @param data
     */
    async addEntityRepository(data: AddProfileParams): Promise<ProfileModel> {
        return await MongoHelper.insertCollection(data, 'profiles')
    }

    /**
     * Esta función hace la búsqueda de un documento en una colección
     * de mongoDB por su ObjectId, que llega como parámetro en la uri
     * @param id
     */
    async loadEntityByIdRepository(id: string): Promise<ILoadEntityByIdRepository.Result> {
        const profile = await MongoHelper.loadCollectionById(id, '_id', 'profiles')
        return profile && MongoHelper.map(profile)
    }

    /**
     * Esta función elimina la colección {profiles} y las colecciones
     * asociadas a ella {posts, users} por el argumento que es común en
     * todas las colecciones.
     * @param userId
     */
    async deleteProfileRepository(userId: string): Promise<void> {
        const profileCollection = await MongoHelper.getCollection('profiles')
        const userCollection = await MongoHelper.getCollection('users')
        const postCollection = await MongoHelper.getCollection('posts')

        try {
            await profileCollection.deleteOne({userId})
            await postCollection.deleteMany({userId})
            await userCollection.deleteOne({_id: new ObjectId(userId)})
        } catch (e) {
            serverError(e)
        }
    }

    /**
     * Esta función agrega un sub-documento a una colección
     * dinámicamente de acuerdo a los parámetros que le envía el
     * controlador[capa infraestructura] al servicio[capa de dominio].
     * @param id
     * @param data
     * @param args
     */
    async addEntitySubDocumentRepository(id: string, data: T, args): Promise<T | void> {
        await MongoHelper.updateCollectionById(id, data, args)
    }


    /**
     * Esta función hace una búsqueda de un documento en una colección o tabla
     * de forma dinámica de acuerdo al argumento que reciba y se puede
     * utilizar con cualquier base de datos.
     *
     * El primer argumento es el {valor} por el que se va a filtrar
     * El segundo argumento es el {campo} por el cual va a buscar en una tabla o colección
     * El tercer argumento es la {tabla} o {colección} donde se hace la búsqueda
     * @param value
     * @param param
     */
    async loadEntityByFieldRepository(value: string, param: string | undefined): Promise<void | boolean | string> {
        return await MongoHelper.loadCollectionByField(value, param, 'profiles')
    }

    /**
     * Esta función elimina un sub-documento de una colección
     * dinámicamente de acuerdo a los parámetros que le envía el
     * controlador[capa infraestructura] al servicio[capa de dominio].
     * @param documentId
     * @param param
     * @param collection
     * @param subDocument
     * @param subDocumentId
     */
    async deleteEntitySubDocumentRepository(documentId: any, param: string, collection: string, subDocument: string,
        subDocumentId: string): Promise<void> {
        await MongoHelper.deleteCollectionById(
            documentId, param, collection,subDocument, subDocumentId
        )
    }
}
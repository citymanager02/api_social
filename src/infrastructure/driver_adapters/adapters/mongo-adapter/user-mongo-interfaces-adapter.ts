import {IUpdateAccessTokenRepository} from "@/domain/models/gateways/auth/update-access-token-repository";
import {ILoadAccountByTokenRepository} from "@/domain/models/gateways/auth/load-account-by-token-repository";
import {IAddEntityRepository} from "@/domain/models/gateways/add-entity-repository";
import {UserModel} from "@/domain/models/user-model";
import {ILoadEntityByIdRepository} from "@/domain/models/gateways/load-entity-by-id-repository";
import {ILoadEntityByFieldRepository} from "@/domain/models/gateways/load-entity-by-field-repository";

export interface UserMongoInterfacesAdapter extends
    ILoadEntityByIdRepository<UserModel>,
    IUpdateAccessTokenRepository,
    ILoadAccountByTokenRepository,
    IAddEntityRepository<UserModel>,
    ILoadEntityByFieldRepository<string>{
}
import {IDeletePostRepository} from "@/domain/models/gateways/posts/delete-post-repository";
import {IAddEntityRepository} from "@/domain/models/gateways/add-entity-repository";
import {ILoadEntityByIdRepository} from "@/domain/models/gateways/load-entity-by-id-repository";
import {ILoadEntitiesRepository} from "@/domain/models/gateways/load-entities-repository";
import {IDeleteEntitySubDocumentRepository} from "@/domain/models/gateways/delete-entity-sub-document-repository";
import {IAddEntitySubDocumentRepository} from "@/domain/models/gateways/add-entity-sub-document-repository";
import {PostModel} from "@/domain/models/post-model";

export interface PostMongoInterfacesAdapter<T> extends IAddEntityRepository<PostModel>,
    IAddEntitySubDocumentRepository<T>,
    ILoadEntityByIdRepository<PostModel>,
    IDeletePostRepository,
    IDeleteEntitySubDocumentRepository,
    ILoadEntitiesRepository<PostModel>{
}
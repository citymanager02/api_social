import {ObjectID} from 'mongodb'
import {PostModel} from "../../../../domain/models/post-model";
import {MongoHelper} from "../../../../infrastructure/driver_adapters/helpers/mongo-helper";
import {PostMongoInterfacesAdapter} from "@/infrastructure/driver_adapters/adapters/mongo-adapter/post-mongo-interfaces-adapter";

/**
 * Clase que provee la comunicación entre el driven-adapters(BD)[capa de infraestructura]
 * y los use-cases(servicios)[capa del dominio] para realizar los diferentes consultas en
 * la base de datos. Las capas se comunican por medio de gateways repositories.
 * @version 1.0
 * @author John Piedrahita
 */
export class PostMongoRepositoryAdapter<T> implements PostMongoInterfacesAdapter<T> {

    /**
     * Esta función retorna un array de posts si existe, en caso contrario
     * un array vacío
     */
    async loadEntitiesRepository(): Promise<PostModel[]> {
        const postCollection = await MongoHelper.getCollection('posts')
        return postCollection.find().toArray()
    }

    /**
     * Esta función hace la búsqueda de un documento en una colección
     * de mongoDB por su ObjectId, que llega como parámetro en la uri
     * @param id
     */
    async loadEntityByIdRepository(id: string): Promise<boolean> {
        const post = await MongoHelper.loadCollectionById(id, '_id','posts')
        return post && MongoHelper.map(post)
    }

    /**
     * Esta función agrega un objeto a una sub-colección de forma dinámica
     * de acuerdo a los parámetros que recibe del servicio(capa del dominio)
     * @param id
     * @param data
     * @param args
     */
    async addEntitySubDocumentRepository(id: string, data: T, args): Promise<T | void> {
        await MongoHelper.updateCollectionById(id, data, args)
    }

    /**
     * Esta función inserta un documento en la colección {posts}, con la data
     * que recibe desde el servicio - caso de uso (capa del dominio)
     * @param data
     */
    async addEntityRepository(data: PostModel): Promise<PostModel> {
        return  await MongoHelper.insertCollection(data, 'posts')
    }

    /**
     * Esta función elimina un documento de la colección {posts}, por la
     * búsqueda del {id}
     * @param id
     */
    async deletePostRepository(id: string): Promise<void> {
        const postCollection = await MongoHelper.getCollection('posts')
        await postCollection.deleteMany({_id: new ObjectID(id)})
    }

    /**
     * Esta función elimina un objeto del array que tiene una colección
     * de forma dinámica de acuerdo a la información que le envía el
     * controlador[capa infraestructura] al servicio[capa de dominio].
     * Nota: Siempre las dependencias van de afuera hacia adentro
     * @param documentId
     * @param param
     * @param collection
     * @param subDocument
     * @param subDocumentId
     */
    async deleteEntitySubDocumentRepository(documentId: string, param: string, collection: string,
                                            subDocument: string, subDocumentId: string): Promise<void> {
        await MongoHelper.deleteCollectionById(documentId, param, collection, subDocument, subDocumentId)
    }
}
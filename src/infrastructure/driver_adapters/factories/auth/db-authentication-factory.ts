import {IAuthentication} from "../../../../domain/models/gateways/auth/authentication-repository";
import {BcryptAdapter} from "../../helpers/bcrypt-adapter";
import {JwtAdapter} from "../../helpers/jwt-adapter";
import {AuthenticationServiceImpl} from "../../../../domain/use_cases/impl/auth/authentication-service-impl";
import {UserMongoRepositoryAdapter} from "../../adapters/mongo-adapter/user-mongo-repository-adapter";
import {SESSION_SECRET} from "../../../../application/config/config";

export const makeDbAuthentication = (): IAuthentication => {
    const salt = 12
    const bcryptAdapter = new BcryptAdapter(salt)
    const jwtAdapter = new JwtAdapter(SESSION_SECRET)
    const userMongoRepository = new UserMongoRepositoryAdapter()

    return new AuthenticationServiceImpl(
        bcryptAdapter,
        jwtAdapter,
        userMongoRepository,
        userMongoRepository,
    )
}
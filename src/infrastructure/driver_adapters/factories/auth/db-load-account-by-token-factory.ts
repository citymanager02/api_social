import {ILoadAccountByTokenService} from "../../../../domain/use_cases/gateways/auth/load-account-by-token-service";
import {JwtAdapter} from "../../helpers/jwt-adapter";
import {SESSION_SECRET} from "../../../../application/config/config";
import {LoadAccountByTokenServiceImpl} from "../../../../domain/use_cases/impl/auth/load-account-by-token-service-impl";
import {UserMongoRepositoryAdapter} from "../../adapters/mongo-adapter/user-mongo-repository-adapter";

export const makeDbLoadAccountByToken = (): ILoadAccountByTokenService => {
    const jwtAdapter = new JwtAdapter(SESSION_SECRET)
    const userRepositoryAdapter = new UserMongoRepositoryAdapter()
    return new LoadAccountByTokenServiceImpl(
        jwtAdapter,
        userRepositoryAdapter
    )
}
import {AddPostServiceImpl} from "../../../../domain/use_cases/impl/posts/add-post-service-impl";
import {PostMongoRepositoryAdapter} from "../../adapters/mongo-adapter/post-mongo-repository-adapter";
import {UserMongoRepositoryAdapter} from "../../adapters/mongo-adapter/user-mongo-repository-adapter";
import {IAddEntityService} from "@/domain/use_cases/gateways/add-entity-service";
import {PostModel} from "@/domain/models/post-model";

export const makeDbAddPost = (): IAddEntityService<PostModel> => {
    const postMongoRepositoryAdapter = new PostMongoRepositoryAdapter()
    const userMongoRepositoryAdapter = new UserMongoRepositoryAdapter()

    return new AddPostServiceImpl(
        postMongoRepositoryAdapter,
        userMongoRepositoryAdapter
    )
}
import {PostMongoRepositoryAdapter} from "../../adapters/mongo-adapter/post-mongo-repository-adapter";
import {ILoadEntityByIdService} from "@/domain/use_cases/gateways/load-entity-by-id-service";
import {PostModel} from "@/domain/models/post-model";
import {LoadEntityDocumentByIdServiceImpl} from "../../../../domain/use_cases/impl/load-entity-document-by-id-service-impl";

export const makeDbLoadPostById = (): ILoadEntityByIdService<PostModel> => {
    const postMongoRepositoryAdapter = new PostMongoRepositoryAdapter()

    return new LoadEntityDocumentByIdServiceImpl(
        postMongoRepositoryAdapter
    )
}
import {PostMongoRepositoryAdapter} from "../../adapters/mongo-adapter/post-mongo-repository-adapter";
import {PostCommentsModel} from "@/domain/models/post-model";
import {AddEntitySubDocumentServiceImpl} from "../../../../domain/use_cases/impl/add-entity-sub-document-service-impl";
import {IAddEntitySubDocumentService} from "../../../../domain/use_cases/gateways/add-entity-sub-document-service";

export const makeDbAddPostComment = (): IAddEntitySubDocumentService<PostCommentsModel> => {
    const postMongoRepositoryAdapter = new PostMongoRepositoryAdapter()

    return new AddEntitySubDocumentServiceImpl(
        postMongoRepositoryAdapter,
        postMongoRepositoryAdapter
    )
}
import {IDeletePostService} from "../../../../domain/use_cases/gateways/posts/delete-post-service";
import {DeletePostServiceImpl} from "../../../../domain/use_cases/impl/posts/delete-post-service-impl";
import {PostMongoRepositoryAdapter} from "../../adapters/mongo-adapter/post-mongo-repository-adapter";

export const makeDbDeletePost = (): IDeletePostService => {
    const postMongoRepositoryAdapter = new PostMongoRepositoryAdapter()

    return new DeletePostServiceImpl(
        postMongoRepositoryAdapter,
        postMongoRepositoryAdapter
    )
}
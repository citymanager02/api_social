import {PostMongoRepositoryAdapter} from "../../adapters/mongo-adapter/post-mongo-repository-adapter";
import {IAddEntitySubDocumentService} from "@/domain/use_cases/gateways/add-entity-sub-document-service";
import {AddEntitySubDocumentServiceImpl} from "../../../../domain/use_cases/impl/add-entity-sub-document-service-impl";
import {PostLikesModel} from "@/domain/models/post-model";

export const makeDbAddPostLike = (): IAddEntitySubDocumentService<PostLikesModel> => {
    const postMongoRepositoryAdapter = new PostMongoRepositoryAdapter()

    return new AddEntitySubDocumentServiceImpl(
        postMongoRepositoryAdapter,
        postMongoRepositoryAdapter
    )
}
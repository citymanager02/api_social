import {PostMongoRepositoryAdapter} from "../../adapters/mongo-adapter/post-mongo-repository-adapter";
import {IDeleteEntitySubDocumentService} from "@/domain/use_cases/gateways/delete-entity-sub-document-service";
import {DeleteEntitySubDocumentServiceImpl} from "../../../../domain/use_cases/impl/delete-entity-sub-document-service-impl";

export const makeDbDeletePostComment = (): IDeleteEntitySubDocumentService => {
    const postMongoRepositoryAdapter = new PostMongoRepositoryAdapter()

    return new DeleteEntitySubDocumentServiceImpl(
        postMongoRepositoryAdapter,
        postMongoRepositoryAdapter,
    )
}
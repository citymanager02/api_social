import {LoadPostsServiceImpl} from "../../../../domain/use_cases/impl/posts/load-posts-service-impl";
import {PostMongoRepositoryAdapter} from "../../adapters/mongo-adapter/post-mongo-repository-adapter";
import {ILoadEntitiesService} from "@/domain/use_cases/gateways/load-entities-service";
import {PostModel} from "@/domain/models/post-model";

export const makeDbLoadPosts = (): ILoadEntitiesService<PostModel> => {
    const postMongoRepositoryAdapter = new PostMongoRepositoryAdapter()

    return new LoadPostsServiceImpl(postMongoRepositoryAdapter)
}
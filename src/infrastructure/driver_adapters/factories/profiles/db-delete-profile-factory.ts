import {DeleteProfileServiceImpl} from "../../../../domain/use_cases/impl/profiles/delete-profile-service-impl";
import {ProfileMongoRepositoryAdapter} from "../../adapters/mongo-adapter/profile-mongo-repository-adapter";
import {ILoadEntityByFieldService} from "@/domain/use_cases/gateways/load-entity-by-field-service";

export const makeDbDeleteProfile = (): ILoadEntityByFieldService<string> => {
    const profileMongoRepositoryAdapter = new ProfileMongoRepositoryAdapter()

    return new DeleteProfileServiceImpl(
        profileMongoRepositoryAdapter,
        profileMongoRepositoryAdapter
    )
}
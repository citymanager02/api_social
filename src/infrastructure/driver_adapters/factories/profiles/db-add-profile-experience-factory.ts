import {ProfileMongoRepositoryAdapter} from "../../adapters/mongo-adapter/profile-mongo-repository-adapter";
import {IAddEntitySubDocumentService} from "@/domain/use_cases/gateways/add-entity-sub-document-service";
import {AddEntitySubDocumentServiceImpl} from "../../../../domain/use_cases/impl/add-entity-sub-document-service-impl";
import {ProfileExperienceModel} from "@/domain/models/profile-model";

export const makeDbAddExperienceProfile = (): IAddEntitySubDocumentService<ProfileExperienceModel> => {
    const profileMongoRepositoryAdapter = new ProfileMongoRepositoryAdapter()

    return new AddEntitySubDocumentServiceImpl(
        profileMongoRepositoryAdapter,
        profileMongoRepositoryAdapter,
    )
}
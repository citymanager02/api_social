import {ProfileMongoRepositoryAdapter} from "../../adapters/mongo-adapter/profile-mongo-repository-adapter";
import {ILoadEntityByIdService} from "@/domain/use_cases/gateways/load-entity-by-id-service";
import {ProfileModel} from "@/domain/models/profile-model";
import {LoadEntityDocumentByIdServiceImpl} from "../../../../domain/use_cases/impl/load-entity-document-by-id-service-impl";

export const makeDbLoadProfileById = (): ILoadEntityByIdService<ProfileModel> => {
    const profileMongoRepositoryAdapter = new ProfileMongoRepositoryAdapter()

    return new LoadEntityDocumentByIdServiceImpl(
        profileMongoRepositoryAdapter
    )
}
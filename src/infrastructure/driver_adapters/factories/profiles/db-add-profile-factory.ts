import {ProfileMongoRepositoryAdapter} from "../../adapters/mongo-adapter/profile-mongo-repository-adapter";
import {AddProfileServiceImpl} from "../../../../domain/use_cases/impl/profiles/add-profile-service-impl";
import {UserMongoRepositoryAdapter} from "../../adapters/mongo-adapter/user-mongo-repository-adapter";
import {IAddEntityService} from "@/domain/use_cases/gateways/add-entity-service";
import {ProfileModel} from "@/domain/models/profile-model";

export const makeDbAddProfile = (): IAddEntityService<ProfileModel> => {
    const profileMongoRepositoryAdapter = new ProfileMongoRepositoryAdapter()
    const userMongoRepositoryAdapter = new UserMongoRepositoryAdapter()
    return new AddProfileServiceImpl(
        profileMongoRepositoryAdapter,
        userMongoRepositoryAdapter,
        profileMongoRepositoryAdapter,
    )
}
import {ProfileMongoRepositoryAdapter} from "../../adapters/mongo-adapter/profile-mongo-repository-adapter";
import {IDeleteEntitySubDocumentService} from "@/domain/use_cases/gateways/delete-entity-sub-document-service";
import {DeleteEntitySubDocumentServiceImpl} from "../../../../domain/use_cases/impl/delete-entity-sub-document-service-impl";

export const makeDbDeleteEducationProfile = (): IDeleteEntitySubDocumentService => {
    const profileMongoRepositoryAdapter = new ProfileMongoRepositoryAdapter()

    return new DeleteEntitySubDocumentServiceImpl(
        profileMongoRepositoryAdapter,
        profileMongoRepositoryAdapter
    )
}
import {UserMongoRepositoryAdapter} from "../../adapters/mongo-adapter/user-mongo-repository-adapter";
import {ILoadEntityByIdService} from "@/domain/use_cases/gateways/load-entity-by-id-service";
import {UserModel} from "@/domain/models/user-model";
import {LoadEntityDocumentByIdServiceImpl} from "../../../../domain/use_cases/impl/load-entity-document-by-id-service-impl";

/**
 * Esta función {@code makeDbLoadUserById} provee la comunicación con el servicio para buscar un
 * usuario por el id, recibe del driven-adapters el gateway {@code ILoadEntityByIdRepository} que
 * lo suministra la clase {@code UserMongoRepositoryAdapter}, se inyecta como dependencia a la
 * implementación del servicio {@code LoadEntityDocumentByIdServiceImpl} que se encuentra en la capa
 * del dominio (use-cases).
 * @version 1.0
 * @author John Piedrahita
 *
 * @return una instancia para la comunicación entre el servicio {@code LoadEntityDocumentByIdServiceImpl}
 *  y la clase {@code UserMongoRepositoryAdapter} quien provee el método {@code loadEntityByIdRepository}
 *  que se inyecta como dependencia por medio del constructor a traves del gateway
 *  {@code ILoadEntityByIdRepository}
 */
export const makeDbLoadUserById = (): ILoadEntityByIdService<UserModel> => {
    const userMongoRepositoryAdapter = new UserMongoRepositoryAdapter()
    return  new LoadEntityDocumentByIdServiceImpl(userMongoRepositoryAdapter)
}
import {BcryptAdapter} from "../../helpers/bcrypt-adapter";
import {UserMongoRepositoryAdapter} from "../../adapters/mongo-adapter/user-mongo-repository-adapter";
import {AddUserServiceImpl} from "../../../../domain/use_cases/impl/users/add-user-service-impl";
import {IAddEntityService} from "@/domain/use_cases/gateways/add-entity-service";
import {UserModel} from "@/domain/models/user-model";

/**
 * Con esta funcion hacemos la Inversion de Dependencia entre la capa de dominio y
 * la capa de infraestructura por medio de la interfaz IAddUserService, al instanciar
 * la clase AddUserServiceImpl debemos injectar las dependencias correspondientes para
 * su implementacion.
 *
 * 1. IHash => BcryptAdapter
 * 2. IAddUserRepository => UserMongoRepositoryAdapter
 */
export const makeDbAddUser = (): IAddEntityService<UserModel> => {
    const salt = 12
    const bcryptAdapter = new BcryptAdapter(salt)
    const userMongoRepositoryAdapter = new UserMongoRepositoryAdapter()

    return new AddUserServiceImpl(
        userMongoRepositoryAdapter,
        userMongoRepositoryAdapter,
        bcryptAdapter
    )
}
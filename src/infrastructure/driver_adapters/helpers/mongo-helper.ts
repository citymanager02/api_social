import {Collection, MongoClient, ObjectId} from "mongodb";

/**
 * Helper que provee el manejo transaccional con la base de datos,
 * en este caso Mongo DB, se utiliza de forma nativa para no depender
 * de un ORM y asi tener mas control de todos los procesos.
 * @version 1.0
 * @author John Piedrahita
 */
export const MongoHelper = {
    client: null as MongoClient,
    uri: null as string,

    /**
     * Esta función nos crea la conexión con la base de datos,
     * recibe como parámetro la uri, previamente configurada en el .env
     * @param uri
     */
    async connect(uri: string): Promise<void> {
        this.uri = uri
        this.client = await MongoClient.connect(uri, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        })
    },

    /**
     * Esta función nos cierra la conexión después de una transacción
     */
    async disconnect(): Promise<void> {
        await this.client.close()
        this.client = null
    },

    /**
     * Esta función recibe el nombre de la colección a la cual
     * vamos a ocupar en una consulta pasamos el nombre como argumento.
     * @param name
     */
    async getCollection(name: string): Promise<Collection> {
        // Si no hay conexión a la base de datos, reintentamos la operación y
        // asi garantizamos que no haya una excepción por parte de la conexión
        if (!this.client?.isConnected()) {
            await this.connect(this.uri)
        }
        return this.client.db().collection(name)
    },

    /**
     * Esta función filtra un documento dentro de una colección de forma
     * dinámica de acuerdo a los argumentos que recibe
     * @param id
     * @param param
     * @param collection
     */
    async loadCollectionById(id: string | number, param: string, collection: string): Promise<any> {
        /**
         * Se crear un objeto vacío para ir pasando los pares de clave/valor de forma independiente
         * mediante notación tradicional en lugar de literal.
         * @return { param: id }
         */
        let objectFilter = {};
        objectFilter[param] = new ObjectId(id)

        /**
         * Estructura para hacer la búsqueda de un documento
         * @return collectionResult.findOne({ param : id })
         */
        const collectionResult = await MongoHelper.getCollection(collection)
        return await collectionResult.findOne(objectFilter)
    },

    /**
     * Esta función filtra un documento dentro de una colección de forma
     * dinámica de acuerdo a los argumentos que recibe
     * @param value
     * @param param
     * @param collection
     */
    async loadCollectionByField(value: string, param: string, collection: string): Promise<any> {
        /**
         * Se crear un objeto vacío para ir pasando los pares de clave/valor de forma independiente
         * mediante notación tradicional en lugar de literal.
         * @return { param: value }
         */
        let objectFilter = {};
        objectFilter[param] = value

        /**
         * Estructura para hacer la búsqueda de un documento
         * @return collectionResult.findOne({ param : value })
         */
        const collectionResult = await MongoHelper.getCollection(collection)
        const result =  await collectionResult.findOne(objectFilter)
        return result && MongoHelper.map(result)
    },

    /**
     * Esta función inserta un documento en una colección
     * @param data
     * @param collection
     */
    async insertCollection(data: any, collection: string): Promise<any> {
        const collectionResult = await MongoHelper.getCollection(collection)
        const result = await collectionResult.insertOne(data)
        return result && MongoHelper.map(result.ops[0])
    },

    /**
     * Esta función agrega un sub-documento a una collection dinámicamente,
     * se conforman las estructuras correspondientes para formar la consulta
     * @param id
     * @param data
     * @param args
     */
    async updateCollectionById(id: string, data: any, args): Promise<any> {
        // Este objeto crear la estructura para insertar el sub-documento
        // en la colección correspondiente
        let objectFilter = {};
        objectFilter[args[2]] = data

        /**
         * Estructura correcta para insertar un sub-documento
         * en una colección
         * @return collectionResult.findOneAndUpdate({ param: id}, {
         *     $push: { subDocument: data }
         * })
         */
        const collectionResult = await MongoHelper.getCollection(args[1])
        await collectionResult.findOneAndUpdate({
            _id: new ObjectId(id)
        }, {
            $push: objectFilter
        })
    },

    /**
     * Esta función elimina un sub-documento de la collection
     * @param documentId
     * @param collection
     * @param param
     * @param subDocument
     * @param subDocumentId
     */
    async deleteCollectionById(documentId: string, param: string, collection: string,
                               subDocument: string, subDocumentId: string): Promise<void> {

        // Colección de la que se va a eliminar el sub-documento
        const collectionResult = await MongoHelper.getCollection(collection)

        /**
         * Este objeto conforma la estructura clave/valor del sub-documento que
         * se va a eliminar de forma dinámica
         *
         * @param param
         * @param subDocumentId
         *
         * @return { id: '8520bf29-75a7-4326-a64a-469591cd350c' }
         */
        let objectUpdateDeleteSubDocument = {};
        objectUpdateDeleteSubDocument[param] = subDocumentId

        /**
         * Este objeto nos conforma la estructura para eliminar un sub-documento de
         * forma dinámica
         *
         * @param subDocument
         * @param param
         * @return objectResult = { subDocument : { param: id } }
         */
        let objectResult = {}
        objectResult[subDocument] = objectUpdateDeleteSubDocument

        /**
         * @return collectionResult.findOneAndUpdate({ _id: new ObjectId(documentId) },
         *      { $pull: { subDocument : { param: id } }
         *  })
         */
        await collectionResult.updateOne({
            _id: new ObjectId(documentId)
        }, {
            $pull: objectResult
        })
    },

    /**
     * Se crear un objeto vacío para ir pasando los pares de clave/valor de forma independiente
     * mediante notación tradicional en lugar de literal.
     * @param key
     * @param value
     */
    mapObjectFilter: (key, value): any => {
        let objectFilter = {};
        return objectFilter[key] = value
    },

    /**
     * Esta función devuelve el objeto con la estructura correcta,
     * ya que en mongo recibimos el id con subrayado (_id) y en la estructura del dominio lo mapeamos (id)
     * de forma tradicional, para no depender de ninguna base de datos,
     * con esto logramos que la base de datos se acople al modelo del dominio, que es una capa más interna.
     * Las capas mas externas dependen de las internas y no al contrario
     */
    map: (data: any): any => {
        const {_id, ...rest} = data
        return Object.assign({}, rest, {id: _id})
    }
}
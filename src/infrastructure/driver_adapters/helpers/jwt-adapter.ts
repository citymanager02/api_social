import jwt from 'jsonwebtoken'
import {IEncrypt} from "../../../domain/use_cases/helpers/gateways/encrypt";
import {IDecrypt} from "../../../domain/use_cases/helpers/gateways/decrypt";

/**
 * Clase que provee el adaptador de la librería {jsonwebtoken} para validar la autenticación
 * en el sistema.
 * @version 1.0
 * @author John Piedrahita
 */
export class JwtAdapter implements IEncrypt, IDecrypt {
    /**
     * Constructor asigna un valor para generar secret
     * @param secret
     */
   constructor(
       private readonly secret: string
   ) {
   }

    /**
     * Esta función verifica que el texto que llega sea el correcto y
     * asi poder autenticarse en el sistema.
     * @param ciphertext
     */
    async decrypt(ciphertext: string): Promise<string> {
       const plaintext: any =  jwt.verify(ciphertext, this.secret)
       return plaintext
    }

    /**
     * Esta función genera el token al usuario registrado en el sistema.
     * @param plaintext
     */
    async encrypt(plaintext: string): Promise<string> {
       const ciphertext = jwt.sign({ account: plaintext }, this.secret, { expiresIn: "1d"})
       return ciphertext
    }
}
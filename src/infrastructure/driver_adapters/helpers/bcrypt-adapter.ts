import bcrypt from 'bcrypt'
import {IHash} from "@/domain/use_cases/helpers/gateways/hash";
import {IHashCompare} from "@/domain/use_cases/helpers/gateways/hash-compare";

/**
 * Clase que provee el adaptador de la librería {bcrypt} para generar los hash
 * en el sistema.
 * @version 1.0
 * @author John Piedrahita
 */
export class BcryptAdapter implements IHash, IHashCompare {

    /**
     * Constructor asigna un valor para generar salt
     * @param salt
     */
    constructor(
        private readonly salt: number
    ) {
    }

    /**
     * Esta función hace la comparación del password que llega en la petición, con el
     * que se encuentra en la base de datos.
     * @param text
     * @param digest
     */
    async compare(text: string, digest: string): Promise<boolean> {
        const isValid = await bcrypt.compare(text, digest)
        return isValid
    }

    /**
     * Esta función nos encripta el texto que llega como parámetro y le inyectamos
     * por el constructor la variable salt.
     * @param text
     */
    async hash(text: string): Promise<string> {
        const digest = await bcrypt.hash(text, this.salt)
        return digest
    }
}
/**
 * Interfaz que nos provee la comunicación entre los controladores[capa de infraestructura]
 * que son las clases que la implementa y el helper adaptRoute[capa de la aplicación], adaptando
 * la conexión con las rutas del framework(Express), que necesitan un Request y Response para
 * aceptar la petición.
 */
import {HttpRequest, HttpResponse} from "@/infrastructure/helpers/http";

export interface IController {
    handle: (request: HttpRequest) => Promise<HttpResponse>
}
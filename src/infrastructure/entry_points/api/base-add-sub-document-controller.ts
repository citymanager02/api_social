import {v4 as uuidV4} from "uuid"
import {IController} from "../gateways/controller";
import {
    badRequest,
    HttpRequest, HttpResponse, noContent, notFound, serverError, unprocessableEntity
} from "../../helpers/http";
import {IAddEntitySubDocumentService} from "@/domain/use_cases/gateways/add-entity-sub-document-service";
import {fieldsValidation} from "../../helpers/fields-validation";

/**
 * Esta clase nos provee el controlador genérico para agregar un sub documento en una colección.
 *
 * @version 1.0
 * @author John Piedrahita
 */
export class BaseAddSubDocumentController<T> implements IController {

    /**
     * Recibimos por constructor los atributos que sirven de configuración para
     * determinar en que colección se insertará el sub documento.
     *
     * @param attributes
     * @param addEntitySubDocumentService
     */
    constructor(
        private readonly attributes: string[],
        private readonly addEntitySubDocumentService: IAddEntitySubDocumentService<T>
    ) {
        this.attributes = attributes
    }

    async handle(request: HttpRequest): Promise<HttpResponse> {

        try {
            // Parámetro correspondiente al id del documento principal
            const {id} = request.params

            // Se valida que el argumento no lleguen vacíos
            const {errors, isValid} = fieldsValidation(request.body)

            // Cuando la validación devuelva un error, mostramos el mensaje
            // correspondiente al consumidor de la API
            if (!isValid) return unprocessableEntity(errors)

            /**
             * @param id
             * @param data
             * @param args
             */
            const subDocument = await this.addEntitySubDocumentService.addEntitySubDocumentService(
                id, { id: uuidV4(), ...request.body }, this.attributes
            )

            if (subDocument === false) return badRequest("El usuario ya dio like a este post")

            // Si el servicio (caso de uso) nos devuelve null, mostramos el mensaje
            // correspondiente al consumidor del API
            if (subDocument === null) return  notFound()

            // Al insertar el registro se retorna un status 204
            return noContent()
        } catch (e) {
            return serverError(e)
        }
    }
}

import {IController} from "../../gateways/controller";
import {
    badRequest, HttpRequest, HttpResponse, notFound, ok, serverError, unprocessableEntity
} from "../../../helpers/http";
import {fieldsValidation} from "../../../helpers/fields-validation";
import {IAddEntityService} from "@/domain/use_cases/gateways/add-entity-service";
import {ProfileModel} from "@/domain/models/profile-model";

/**
 * Clase que provee el controlador y actúa como un puente entre la [capa del dominio],
 * hacia adentro y la [capa de la aplicación], hacia afuera, generando la comunicación
 * para insertar un ProfileModel en una colección o tabla [use-cases => capa del dominio]
 * y comunica el controlador con el helper adaptRoute [capa de la aplicación], la cual
 * hace su implementación en las rutas.
 * @version 1.0
 * @author John Piedrahita
 */
export class AddProfileController implements IController {
    /**
     * Constructor asigna un valor al gateway genérico para insertar un documento
     * @param addProfileService Recibe el valor a traves del constructor
     */
    constructor(private readonly addProfileService: IAddEntityService<ProfileModel>) {
    }

    /**
     * Esta función se encarga de recibir el request de la petición, hacer las validaciones
     * correspondientes para enviar los datos al caso de uso, a través de la gateway {IAddEntityService}
     * la cual hace la comunicación entre el controlador(capa de infraestructura) y el caso de uso
     * (capa del dominio) donde esta la implementación.
     * @param request
     */
    async handle(request: HttpRequest): Promise<HttpResponse> {
        try {

            // Campos a validar, la petición trae mas campos pero los otros son opcionales
            const {skills, status, userId} = request.body

            // Esta función solo hace la validación de los parámetros que se detalle en la petición
            const {errors, isValid} = fieldsValidation({skills, status, userId})

            // Cuando la validación devuelva un error, mostramos el mensaje
            // correspondiente al usuario.
            if (!isValid) return unprocessableEntity(errors)

            // Los skills llegan como un string, se hace la transformación a un array
            // para guardarlo en la BD.
            const skillsArray = skills.split(',').map((skill) => skill.trim())

            /**
             * Enviamos por medio de la interfaz {IAddEntityService} los argumentos
             * al caso de uso, estos parámetros son dinámicos y se envían de acuerdo
             * a la necesidad del servicio(caso de uso).
             * @param data
             */
            const profile = await this.addProfileService.addEntityService({
                ...request.body,
                skills: skillsArray,
                date: new Date()
            })

            // Si el caso de uso nos devuelve null retornamos el mensaje de error
            // al consumidor del API.
            if (profile === null) return notFound()

            // Si el caso de uso nos devuelve false retornamos el mensaje de error
            // al consumidor del API.
            if (profile === false)
                return badRequest("El usuario ya esta vinculado a un perfil en el sistema")

            // Al insertar el registro se retorna el detalle del perfil.
            return ok(profile)

        } catch (e) {
            return serverError(e)
        }
    }
}
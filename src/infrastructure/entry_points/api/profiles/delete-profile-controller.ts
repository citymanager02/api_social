import {IController} from "../../../../infrastructure/entry_points/gateways/controller";
import {badRequest, HttpRequest, HttpResponse, noContent, serverError} from "../../../../infrastructure/helpers/http";
import {ILoadEntityByFieldService} from "@/domain/use_cases/gateways/load-entity-by-field-service";

/**
 * Clase que provee el controlador y actúa como un puente entre la [capa del dominio],
 * hacia adentro y la [capa de la aplicación], hacia afuera, generando la comunicación
 * para eliminar un ProfileModel en una colección o una tabla por el id del perfil y los,
 * modelos asociados al ProfileModel [use-cases => capa del dominio] y comunica el
 * controlador con el helper adaptRoute [capa de la aplicación], la cual hace su
 * implementación en las rutas.
 * @version 1.0
 * @author John Piedrahita
 *
 * @return noContent
 */
export class DeleteProfileController implements IController {
    /**
     * Constructor asigna un valor al gateway genérico para eliminar el modelo
     * @param loadProfileByFieldService
     */
    constructor(
        private readonly loadProfileByFieldService: ILoadEntityByFieldService<string>
    ) {
    }

    /**
     * Esta función se encarga de recibir el parámetro {userId} que llegan en el request de la petición,
     * enviarlo al caso de uso, a través de la gateway genérica {ILoadEntityByFieldService} la cual hace
     * la comunicación entre el controlador(capa de infraestructura) y el caso de uso (capa del dominio)
     * donde esta la implementación.
     */
    async handle(request: HttpRequest): Promise<HttpResponse> {
        try {
            // Parámetro correspondiente al id del usuario asociado al perfil
            const {userId} = request.params

            /**
             * Enviamos por medio de la interfaz {ILoadEntityByFieldService} el argumento
             * al caso de uso, estos parámetros son dinámicos y se envían de acuerdo a la
             * necesidad del servicio(caso de uso)
             * @param userId
             */
            const profile = await this.loadProfileByFieldService.loadEntityFieldService(
                userId, 'userId'
            )

            // Si el caso de uso nos devuelve null retornamos el mensaje de error
            // al consumidor del API
            if (profile === null)
                return badRequest("El Perfil que va a eliminar no existe")

            // Al eliminar el registro se retorna un status 204
            return noContent()

        } catch (e) {
            return serverError(e)
        }
    }
}
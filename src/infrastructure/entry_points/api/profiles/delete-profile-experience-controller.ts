import {IController} from "../../gateways/controller";
import {badRequest, HttpRequest, HttpResponse, noContent, serverError} from "../../../helpers/http";
import {IDeleteEntitySubDocumentService} from "@/domain/use_cases/gateways/delete-entity-sub-document-service";

/**
 * Clase que provee el controlador y actúa como un puente entre la [capa del dominio],
 * hacia adentro y la [capa de la aplicación], hacia afuera, generando la comunicación
 * para eliminar un ProfileExperienceModel en la colección por el id del perfil y
 * el id del comentario(servicio) [use-cases => capa del dominio] y comunica el controlador
 * con el helper adaptRoute [capa de la aplicación], la cual hace su implementación en las rutas.
 * @version 1.0
 * @author John Piedrahita
 */
export class DeleteProfileExperienceController implements IController {
    /**
     * Constructor asigna un valor al gateway genérico para eliminar el modelo
     * @param deleteProfileExperienceService Recibe el valor a traves del constructor
     */
    constructor(
        private readonly deleteProfileExperienceService: IDeleteEntitySubDocumentService
    ) {
    }

    /**
     * Esta función se encarga de recibir los parámetros en el request de la petición,
     * {profileId, experienceId} y enviarlos al caso de uso, a través del gateway
     * {IDeleteEntitySubDocumentService} donde esta la implementación.
     * @param request
     */
    async handle(request: HttpRequest): Promise<HttpResponse> {

        try {
            // Parámetros correspondientes al id del perfil y al id de la experiencia
            const {profileId, experienceId} = request.params

            /**
             * Enviamos por medio del gateway {IDeleteEntitySubDocumentService} los argumentos
             * al caso de uso, estos parámetros son dinámicos y se envían de acuerdo
             * a la necesidad del servicio(caso de uso)
             * @param documentId
             * @param param
             * @param collection
             * @param subDocument
             * @param subDocumentId
             */
            const experience = await this.deleteProfileExperienceService.deleteEntitySubDocumentService(
                profileId, 'id', 'profiles', 'experience', experienceId
            )

            // Si el caso de uso nos devuelve null retornamos el mensaje de error
            // al consumidor del API
            if (experience === null)
                return badRequest("El Perfil al que intenta eliminar la experiencia no existe")

            // Al eliminar el registro se retorna un status 204
            return noContent()

        } catch (e) {
            return serverError(e)
        }
    }
}
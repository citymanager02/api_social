import {IController} from "../../gateways/controller";
import {
    HttpRequest, HttpResponse, ok, serverError, unauthorized, unprocessableEntity
} from "../../../helpers/http";
import {IAuthentication} from "../../../../domain/models/gateways/auth/authentication-repository";
import {fieldsValidation} from "../../../helpers/fields-validation";

/**
 * Clase que provee el controlador y actúa como un puente entre la [capa del dominio], hacia adentro
 * y la [capa de la aplicación], hacia afuera, generando la comunicación para autenticar un UserModel
 * mediante el servicio [use-cases => capa del dominio] y comunica el controlador con el helper
 * adaptRoute [capa de la aplicación], la cual hace su implementación en las rutas.
 * @version 1.0
 * @author John Piedrahita
 */
export class LoginController implements IController {
    /**
     * Constructor asigna la data al gateway para hacer la autenticación del modelo
     * @param authentication
     */
    constructor(
        private readonly authentication: IAuthentication) {
    }

    /**
     * Esta función se encarga de recibir el request de la petición, hacer las validaciones
     * correspondientes para enviar los datos al caso de uso, a través de la gateway {IAuthentication}
     * y hacer la comunicación entre el controlador(capa de infraestructura) y el caso de uso
     * (capa del dominio) donde esta la implementación.
     * @param request
     */
    async handle(request: HttpRequest): Promise<HttpResponse> {

        try {
            // Hacemos la validación de los parámetros que lleguen en la petición
            // para garantizar que no lleguen vacíos o nulos
            const {errors, isValid} = fieldsValidation(request.body)

            // Si hay un error en la petición retornamos el mensaje del error
            // y el statusCode 422
            if (!isValid) return unprocessableEntity(errors)

            // Destructuring del request
            const {email, password} = request.body

            // Pasamos los parámetros de la autenticación
            const authenticationModel = await this.authentication.auth({email, password})

            // Si el caso de uso nos devuelve null retornamos el mensaje de error
            // al consumidor del API
            if (!authenticationModel) return unauthorized()

            // Cuando la autenticación es correcta, retornamos el accessToken
            return ok(authenticationModel)

        } catch (e) {
            return serverError(e)
        }
    }
}
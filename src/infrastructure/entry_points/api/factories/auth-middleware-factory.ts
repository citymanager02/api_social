import {IMiddleware} from "../../gateways/middleware";
import {AuthMiddleware} from "../../../../application/middlewares/auth-middleware";
import {makeDbLoadAccountByToken} from "../../../driver_adapters/factories/auth/db-load-account-by-token-factory";

export const makeAuthMiddleware = (): IMiddleware => {
    return new AuthMiddleware(makeDbLoadAccountByToken())
}
import {IController} from "../../../entry_points/gateways/controller";
import {BaseAddSubDocumentController} from "../../api/base-add-sub-document-controller";
import {makeDbAddPostComment} from "../../../../infrastructure/driver_adapters/factories/posts/db-add-post-comment-factory";
import {makeDbAddPostLike} from "../../../../infrastructure/driver_adapters/factories/posts/db-add-post-like-factory";
import {makeDbAddExperienceProfile} from "../../../../infrastructure/driver_adapters/factories/profiles/db-add-profile-experience-factory";
import {makeDbAddEducationProfile} from "../../../../infrastructure/driver_adapters/factories/profiles/db-add-profile-education-factory";
import {
    ADD_POSTS,
    ADD_POSTS_COMMENTS,
    ADD_POSTS_LIKES,
    ADD_PROFILES_EDUCATIONS,
    ADD_PROFILES_EXPERIENCES, ADD_USERS,
    DELETE_POSTS,
    DELETE_POSTS_COMMENTS,
    DELETE_POSTS_LIKES, DELETE_PROFILE,
    DELETE_PROFILE_EDUCATION,
    DELETE_PROFILE_EXPERIENCES, LOAD_POSTS, LOAD_POSTS_BY_ID, LOAD_PROFILE_BY_ID, LOAD_USER_BY_ID, LOGIN
} from "../../../../infrastructure/helpers/constants";
import {AddPostController} from "../../../../infrastructure/entry_points/api/posts/add-post-controller";
import {makeDbAddPost} from "../../../../infrastructure/driver_adapters/factories/posts/db-add-post-factory";
import {DeletePostController} from "../../../../infrastructure/entry_points/api/posts/delete-post-controller";
import {makeDbDeletePost} from "../../../../infrastructure/driver_adapters/factories/posts/db-delete-post-factory";
import {AddUserController} from "../../../../infrastructure/entry_points/api/users/add-user-controller";
import {makeDbAddUser} from "../../../../infrastructure/driver_adapters/factories/users/db-add-user-factory";
import {DeleteProfileController} from "../../../../infrastructure/entry_points/api/profiles/delete-profile-controller";
import {makeDbDeleteProfile} from "../../../../infrastructure/driver_adapters/factories/profiles/db-delete-profile-factory";
import {DeletePostCommentController} from "../../../../infrastructure/entry_points/api/posts/delete-post-comment-controller";
import {makeDbDeletePostComment} from "../../../../infrastructure/driver_adapters/factories/posts/db-delete-post-comment-factory";
import {DeletePostLikeController} from "../../../../infrastructure/entry_points/api/posts/delete-post-like-controller";
import {makeDbDeletePostLike} from "../../../../infrastructure/driver_adapters/factories/posts/db-delete-post-like-factory";
import {DeleteProfileEducationController} from "../../../../infrastructure/entry_points/api/profiles/delete-profile-education-controller";
import {makeDbDeleteEducationProfile} from "../../../../infrastructure/driver_adapters/factories/profiles/db-delete-profile-education-factory";
import {DeleteProfileExperienceController} from "../../../../infrastructure/entry_points/api/profiles/delete-profile-experience-controller";
import {makeDbDeleteExperienceProfile} from "../../../../infrastructure/driver_adapters/factories/profiles/db-delete-profile-experience-factory";
import {LoadPostByIdController} from "../../../../infrastructure/entry_points/api/posts/load-post-by-id-controller";
import {makeDbLoadPostById} from "../../../../infrastructure/driver_adapters/factories/posts/db-load-post-by-id-factory";
import {LoadProfileByIdController} from "../../../../infrastructure/entry_points/api/profiles/load-profile-by-id-controller";
import {makeDbLoadProfileById} from "../../../../infrastructure/driver_adapters/factories/profiles/db-load-profile-by-id-factory";
import {LoadUserByIdController} from "../../../../infrastructure/entry_points/api/users/load-user-by-id-controller";
import {makeDbLoadUserById} from "../../../../infrastructure/driver_adapters/factories/users/db-load-user-by-id-factory";
import {LoadPostsController} from "../../../../infrastructure/entry_points/api/posts/load-posts-controller";
import {makeDbLoadPosts} from "../../../../infrastructure/driver_adapters/factories/posts/db-load-posts-factory";
import {LoginController} from "../../../../infrastructure/entry_points/api/auth/login-controller";
import {makeDbAuthentication} from "../../../../infrastructure/driver_adapters/factories/auth/db-authentication-factory";

/**
 * Esta función nos provee la instancia para agregar un sub documento en una colección.
 * Inyecta al controlador las dependencia que luego se transmiten al servicio,
 *
 * @version 1.0
 * @author John Piedrahita
 */
export const makeBaseControllerFactory = (type: string): IController => {
    switch (type) {
        /**
         * ================ AGREGAR UN SUB DOCUMENTO A UN COLECCIÓN ===================
         *
         * Esta instancia espera recibir Array de strings {@code attributes: string['documentId', 'collection', 'subDocument']}
         * con la configuración para la inserción en la bade de datos y el factory correspondiente a cada caso,
         * de tipo {@code IAddEntitySubDocumentService}, este gateway hace la comunicación con el servicio por
         * medio de la Inversión de Dependencias.
         */
        case ADD_POSTS_COMMENTS:
            return new BaseAddSubDocumentController(
                ['id', 'posts', 'comments'], makeDbAddPostComment()
            )
        case ADD_POSTS_LIKES:
            return new BaseAddSubDocumentController(
                ['id', 'posts', 'likes'], makeDbAddPostLike()
            )
        case ADD_PROFILES_EXPERIENCES:
            return new BaseAddSubDocumentController(
                ['id', 'profiles', 'experiences'], makeDbAddExperienceProfile()
            )
        case ADD_PROFILES_EDUCATIONS:
            return new BaseAddSubDocumentController(
                ['id', 'profiles', 'educations'], makeDbAddEducationProfile()
            )
        /**
         * ====================== AGREGAR UN DOCUMENTO ================================
         */
        case ADD_POSTS:
            return new AddPostController(makeDbAddPost()
            )
        case ADD_USERS:
            return new AddUserController(makeDbAddUser()
            )

        /**
         * ====================== BORRAR UN DOCUMENTO =================================
         */
        case DELETE_POSTS:
            return new DeletePostController(makeDbDeletePost()
            )
        case DELETE_PROFILE:
            return new DeleteProfileController(makeDbDeleteProfile()
            )

        /**
         * ======================= BORRAR UN SUB DOCUMENTO DE UNA COLECCIÓN ===========
         */
        case DELETE_POSTS_COMMENTS:
            return new DeletePostCommentController(makeDbDeletePostComment()
            )
        case DELETE_POSTS_LIKES:
            return new DeletePostLikeController(makeDbDeletePostLike()
            )
        case DELETE_PROFILE_EDUCATION:
            return new DeleteProfileEducationController(makeDbDeleteEducationProfile()
            )
        case DELETE_PROFILE_EXPERIENCES:
            return new DeleteProfileExperienceController(makeDbDeleteExperienceProfile()
            )

        /**
         * =========================== BUSCAR UNA DOCUMENTO POR EL ID =================
         */
        case LOAD_POSTS_BY_ID:
            return new LoadPostByIdController(makeDbLoadPostById()
            )
        case LOAD_PROFILE_BY_ID:
            return new LoadProfileByIdController(makeDbLoadProfileById()
            )
        case LOAD_USER_BY_ID:
            return new LoadUserByIdController(makeDbLoadUserById()
        )

        /**
         * ============================= BUSCAR TODOS LOS DOCUMENTOS ==================
         */
        case LOAD_POSTS:
            return new LoadPostsController(makeDbLoadPosts()
            )

        /***
         * ============================== AUTH ========================================
         */
        case LOGIN:
            return new LoginController(makeDbAuthentication()
            )
    }
}
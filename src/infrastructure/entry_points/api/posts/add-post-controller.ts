import {IController} from "../../gateways/controller";
import {
    badRequest, HttpRequest, HttpResponse, ok, serverError, unprocessableEntity
} from "../../../helpers/http";
import {fieldsValidation} from "../../../helpers/fields-validation";
import {IAddEntityService} from "@/domain/use_cases/gateways/add-entity-service";
import {PostModel} from "@/domain/models/post-model";

/**
 * Clase que provee el controlador y actúa como un puente entre la [capa del dominio],
 * hacia adentro y la [capa de la aplicación], hacia afuera, generando la comunicación
 * para insertar un PostModel en una colección o tabla [use-cases => capa del dominio]
 * y comunica el controlador con el helper adaptRoute [capa de la aplicación], la cual
 * hace su implementación en las rutas.
 * @version 1.0
 * @author John Piedrahita
 */
export class AddPostController implements IController {
    /**
     * Constructor asigna un valor al gateway genérico para insertar un documento
     * @param addPostService Recibe el valor a traves del constructor
     */
    constructor(
        private readonly addPostService: IAddEntityService<PostModel>
    ) {
    }

    /**
     * Esta función se encarga de recibir el request de la petición, hacer las validaciones
     * de que el request body no llegue vacío para enviar los datos al servicio (caso de uso),
     * a través de la gateway {IAddEntityService} como es genérica recibe el modelo {PostModel}
     * y asi tener una comunicación entre el controlador(capa de infraestructura) y el caso de uso
     * (capa del dominio) donde está la implementación.
     * @param request
     */
    async handle(request: HttpRequest): Promise<HttpResponse> {

        try {
            // Destructuring de las propiedades a validar
            const {text, userId} = request.body

            // Se valida que el argumento no lleguen vacíos
            const {errors, isValid} = fieldsValidation({text, userId})

            // Cuando la validación devuelva un error, mostramos el mensaje
            // correspondiente al consumidor de la API
            if (!isValid) return unprocessableEntity(errors)

            /**
             * Enviamos por medio del gateway {IAddEntityService} los argumentos
             * al caso de uso, estos parámetros son dinámicos y se envían de acuerdo
             * a la necesidad del servicio(caso de uso).
             * @param data
             */
            const post = await this.addPostService.addEntityService({...request.body, date: new Date()})

            // Si el servicio(caso de uso) nos devuelve false mostramos el mensaje
            // correspondiente al consumidor de la API
            if (post === false) return badRequest("El usuario que intenta crear el Post no existe")

            // Al insertar el registro se retorna un status 200 con el detalle del post
            return ok(post)

        } catch (e) {
            serverError(e)
        }
    }
}
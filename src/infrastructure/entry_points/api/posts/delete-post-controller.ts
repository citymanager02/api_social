import {IController} from "../../gateways/controller";
import {badRequest, HttpRequest, HttpResponse, noContent, serverError} from "../../../helpers/http";
import {IDeletePostService} from "../../../../domain/use_cases/gateways/posts/delete-post-service";

/**
 * Clase que provee el controlador y actúa como un puente entre la [capa del dominio],
 * hacia adentro y la [capa de la aplicación], hacia afuera, generando la comunicación
 * para eliminar un Post en la colección o una tabla (servicio) [use-cases => capa del dominio]
 * y comunica el controlador con el helper adaptRoute [capa de la aplicación], la cual hace
 * su implementación en las rutas.
 * @version 1.0
 * @author John Piedrahita
 */
export class DeletePostController implements IController {
    /**
     * Constructor asigna un valor al gateway genérico para eliminar el modelo
     * @param deletePostService Recibe el valor a traves del constructor
     */
    constructor(
        private readonly deletePostService: IDeletePostService
    ) {
    }

    /**
     * Esta función se encarga de recibir los parámetros que llega en el request de la petición
     * {id}, hacer las validaciones de que exista el {Post}, para poder eliminarlo, enviar los
     * datos al servicio (caso de uso), a través del gateway {IDeletePostService} para tener
     * una comunicación entre el controlador(capa de infraestructura) y el caso de uso
     * (capa del dominio) donde está la implementación.
     * @param request
     */
    async handle(request: HttpRequest): Promise<HttpResponse> {

        try {
            // Parámetro correspondiente al id de la colección posts
            const {id} = request.params

            /**
             * Enviamos por medio del gateway {IDeletePostService} los argumentos
             * al caso de uso, estos parámetros son dinámicos y se envían de acuerdo
             * a la necesidad del servicio(caso de uso)
             * @param id
             */
            const result = await this.deletePostService.deletePostService(id)

            // Si el servicio (caso de uso) nos devuelve null, mostramos el mensaje
            // correspondiente al consumidor del API
            if (result === null)
                return badRequest("El Post que intenta eliminar no existe")

            // Al eliminar el post se retorna un status 204
            return noContent()

        }catch (e) {
            serverError(e)
        }
    }
}
import {IController} from "../../gateways/controller";
import {badRequest, HttpRequest, HttpResponse, noContent, serverError} from "../../../helpers/http";
import {IDeleteEntitySubDocumentService} from "@/domain/use_cases/gateways/delete-entity-sub-document-service";

/**
 * Clase que provee el controlador y actúa como un puente entre la [capa del dominio],
 * hacia adentro y la [capa de la aplicación], hacia afuera, generando la comunicación
 * para eliminar un PostLikeModel en la colección por el id del post y el id del like(servicio)
 * [use-cases => capa del dominio] y comunica el controlador con el helper adaptRoute
 * [capa de la aplicación], la cual hace su implementación en las rutas.
 * @version 1.0
 * @author John Piedrahita
 */
export class DeletePostLikeController implements IController {
    /**
     * Constructor asigna un valor al gateway genérico para eliminar el modelo
     * @param deletePostLikeService Recibe el valor a traves del constructor
     */
    constructor(
        private readonly deletePostLikeService: IDeleteEntitySubDocumentService
    ) {
    }

    /**
     * Esta función se encarga de recibir los parámetros que llega en el request de la petición
     * {postId, likeId}, hacer las validaciones de que exista el {Post} y el {Like} para poder
     * eliminar el like, enviar los datos al servicio (caso de uso), esto pasa a través del gateway
     * {IDeleteEntitySubDocumentService} como es genérica recibe el valor correspondiente y asi
     * tener una comunicación entre el controlador(capa de infraestructura) y el caso de uso
     * (capa del dominio) donde está la implementación.
     * @param request
     */
    async handle(request: HttpRequest): Promise<HttpResponse> {

        try {
            // Parámetro correspondiente al id de la colección posts
            // y el id que se encuentre en el array likes
            const {postId, likeId} = request.params

            /**
             * Enviamos por medio de la interfaz {IDeleteEntitySubDocumentService} los argumentos
             * al caso de uso, estos parámetros son dinámicos y se envían de acuerdo a la necesidad
             * del servicio(caso de uso).
             * @param id
             * @param param
             * @param collection
             * @param subDocument
             * @param subDocumentId
             */
            const like = await this.deletePostLikeService.deleteEntitySubDocumentService(
                postId, 'id', 'posts', 'likes', likeId
            )

            // Si el servicio (caso de uso) nos devuelve null, mostramos el mensaje
            // correspondiente al consumidor del API
            if (like === null)
                return badRequest("El Post al que intenta eliminar el like no existe")

            // Si el servicio (caso de uso) nos devuelve false, mostramos el mensaje
            // correspondiente al consumidor del API
            if (like === false)
                return badRequest("El like no se puede eliminar")

            // Al eliminar el comentario se retorna un status 204
            return noContent()

        } catch (e) {
            serverError(e)
        }
    }
}
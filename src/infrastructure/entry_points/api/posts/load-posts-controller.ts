import {IController} from "../../gateways/controller";
import {HttpRequest, HttpResponse, ok, serverError} from "../../../helpers/http";
import {ILoadEntitiesService} from "@/domain/use_cases/gateways/load-entities-service";
import {PostModel} from "@/domain/models/post-model";

/**
 * Clase que provee el controlador y actúa como un puente entre la [capa del dominio], hacia adentro
 * y la [capa de la aplicación], hacia afuera, generando la comunicación para buscar un PostModel[]
 * en una colección o tabla mediante el servicio [use-cases => capa del dominio] y comunica el controlador
 * con el helper adaptRoute [capa de la aplicación], la cual hace su implementación en las rutas.
 * @version 1.0
 * @author John Piedrahita
 */
export class LoadPostsController implements IController {
    /**
     * Constructor asigna un valor al gateway genérico para hacer la búsqueda del modelo
     * @param loadPostsService Recibe el valor del id a traves del constructor
     */
    constructor(
        private readonly loadPostsService: ILoadEntitiesService<PostModel>
    ) {
    }

    /**
     * Esta función nos devuelve un array con los posts existentes y se comunica
     * al caso de uso a traves del gateway {ILoadEntitiesService} donde esta la
     * implementación.
     * @param request
     */
    async handle(request: HttpRequest): Promise<HttpResponse> {

        try {
            /**
             * Enviamos por medio del gateway {ILoadEntitiesService} los argumentos
             * al caso de uso, estos parámetros son dinámicos y se envían de acuerdo
             * a la necesidad del servicio(caso de uso)
             */
            const posts = await this.loadPostsService.loadEntitiesService()

            // Si los posts existen se retorna un status 200 con el detalle
            return ok(posts)

        } catch (e) {
            serverError(e)
        }
    }
}
import {IController} from "../../gateways/controller";
import {HttpRequest, HttpResponse, notFound, ok, serverError} from "../../../helpers/http";
import {ILoadEntityByIdService} from "@/domain/use_cases/gateways/load-entity-by-id-service";
import {UserModel} from "@/domain/models/user-model";

/**
 * Clase que provee el controlador y actúa como un puente entre la [capa del dominio], hacia adentro
 * y la [capa de la aplicación], hacia afuera, generando la comunicación para buscar un UserModel en una
 * colección o tabla por el id (servicio) [use-cases => capa del dominio] y comunica el controlador
 * con el helper adaptRoute [capa de la aplicación], la cual hace su implementación en las rutas.
 * @version 1.0
 * @author John Piedrahita
 */
export class LoadUserByIdController implements IController {
    /**
     * Constructor asigna un valor al gateway genérico para hacer la búsqueda del modelo
     * @param loadUserByIdService
     */
    constructor(
        private readonly loadUserByIdService: ILoadEntityByIdService<UserModel>
    ) {
    }

    /**
     * Esta función se encarga de recibir los parámetros en el request de la petición,
     * {@code id} y enviarlos al caso de uso, a través del gateway {@code ILoadEntityByIdService}
     * donde esta la implementación.
     * @param request
     *
     * @return un User Model de acuerdo al id correspondiente
     */
    async handle(request: HttpRequest): Promise<HttpResponse> {

        try {
            // Parámetro correspondiente al id del usuario
            const {id} = request.params

            /**
             * Enviamos por medio del gateway {ILoadEntityByIdService} los argumentos
             * al caso de uso, estos parámetros son dinámicos y se envían de acuerdo
             * a la necesidad del servicio(caso de uso)
             * @param id
             */
            const user = await this.loadUserByIdService.loadEntityByIdService(id)

            // Si el caso de uso nos devuelve false retornamos el mensaje de error
            // al consumidor del API.
            if (user === false) return notFound()

            // Si el usuario existe se retorna un status 200 con el detalle
            return ok(user)

        } catch (e) {
            return serverError(e)
        }
    }
}
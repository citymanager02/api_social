import {IController} from "../../gateways/controller";
import {
    forbidden, HttpRequest, HttpResponse, ok, serverError, unprocessableEntity
} from "../../../helpers/http";
import {fieldsValidation} from "../../../helpers/fields-validation";
import {EmailInUseError} from "../../../helpers/errors";
import {IAddEntityService} from "@/domain/use_cases/gateways/add-entity-service";
import {UserModel} from "@/domain/models/user-model";

/**
 * Clase que provee el controlador y actúa como un puente entre la [capa del dominio], hacia adentro
 * y la [capa de la aplicación], hacia afuera, generando la comunicación para insertar un UserModel
 * en una colección o tabla (servicio) [use-cases => capa del dominio] y comunica el controlador
 * con el helper adaptRoute [capa de la aplicación], la cual hace su implementación en las rutas.
 * @version 1.0
 * @author John Piedrahita
 */
export class AddUserController implements IController {
    /**
     * Constructor asigna la data al gateway genérico para hacer la inserción del modelo
     * @param addUserService Recibe la data a traves del constructor
     */
    constructor(private readonly addUserService: IAddEntityService<UserModel>) {
    }

    /**
     * Esta función se encarga de recibir el request de la petición, hacer las validaciones
     * correspondientes y enviar los datos al caso de uso, a través del gateway {IAddEntityService}
     * donde esta la implementación.
     */
    async handle(request: HttpRequest): Promise<HttpResponse> {

        try {
            // Hacemos la validación de los parámetros que lleguen en la petición
            // para garantizar que no lleguen vacíos o nulos
            const {errors, isValid} = fieldsValidation(request.body)

            // Si llega un error devolvemos un status code 422
            if (!isValid) return unprocessableEntity(errors)

            /**
             * Enviamos por medio de la interfaz {IAddEntityService} los argumentos
             * al servicio(caso de uso).
             * @param data
             */
            const user = await this.addUserService.addEntityService({
                ...request.body, date: new Date()
            })

            // Si el caso de uso nos devuelve null retornamos el mensaje de error
            // al consumidor del API
            if (user === null) return forbidden(new EmailInUseError())

            // Al insertar el registro se retorna un status 200 con el detalle del usuario
            return ok(user)

        } catch (error) {
            return serverError(error)
        }
    }
}
import {ILoadEntityByFieldService} from "@/domain/use_cases/gateways/load-entity-by-field-service";
import {ILoadEntityByFieldRepository} from "@/domain/models/gateways/load-entity-by-field-repository";
import {IDeleteProfileRepository} from "@/domain/models/gateways/profiles/delete-profile-repository";

/**
 * Clase que provee el servicio para eliminar un ProfileModel y los modelos asociados
 * asociados.
 * @version 1.0
 * @author John Piedrahita
 */
export class DeleteProfileServiceImpl implements ILoadEntityByFieldService<string, string> {
    /**
     * Constructor asigna un valor a los gateways genéricos
     * @param loadProfileByUserIdRepository
     * @param deleteProfileRepository
     */
    constructor(
        private readonly loadProfileByUserIdRepository: ILoadEntityByFieldRepository<any>,
        private readonly deleteProfileRepository: IDeleteProfileRepository
    ) {
    }

    /**
     * Esta función se encarga de eliminar un perfil y las colecciones asociadas a este por medio
     * de un campo en común {userId} que recibe desde el controlador.
     * @param field
     * @param param
     */
    async loadEntityFieldService(field: any, param: string): Promise<void> {
        // Buscamos que exista el documento principal que se va a eliminar
        const profileExist = await this.loadProfileByUserIdRepository.loadEntityByFieldRepository(field, param)

        // Si no existe se retorna un null al controlador para que maneje la excepción.
        if (!profileExist) return null

        // Si el perfil existe se elimina todas las colecciones asociadas.
        if (profileExist) {
            await this.deleteProfileRepository.deleteProfileRepository(profileExist.userId)
        }
    }
}
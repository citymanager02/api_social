import {ProfileModel} from "@/domain/models/profile-model";
import {IAddEntityRepository} from "@/domain/models/gateways/add-entity-repository";
import {IAddEntityService} from "@/domain/use_cases/gateways/add-entity-service";
import {ILoadEntityByIdRepository} from "@/domain/models/gateways/load-entity-by-id-repository";
import {ILoadEntityByFieldRepository} from "@/domain/models/gateways/load-entity-by-field-repository";
import {UserModel} from "@/domain/models/user-model";

/**
 * Clase que provee el servicio para insertar un ProfileModel en una colección
 * @version 1.0
 * @author John Piedrahita
 */
export class AddProfileServiceImpl implements IAddEntityService<ProfileModel> {
    /**
     * Constructor asigna un valor a los gateway genérico.
     * @param addProfileRepository
     * @param loadUserByIdRepository
     * @param loadProfileByUserId
     */
    constructor(
        private readonly addProfileRepository: IAddEntityRepository<ProfileModel>,
        private readonly loadUserByIdRepository: ILoadEntityByIdRepository<UserModel>,
        private readonly loadProfileByUserId: ILoadEntityByFieldRepository<string>
        ) {
    }

    /**
     * Esta función inserta registros en la colección {profiles}, de acuerdo
     * a la información que llegue en los parámetros desde el controlador[capa infraestructura]
     * por medio de la interfaz genérica {IAddEntityService}
     * @param data
     */
    async addEntityService(data: ProfileModel): Promise<boolean | ProfileModel> {
        // Si el usuario existe se le asigna el perfil que se va a crear
        const userExist = await this.loadUserByIdRepository.loadEntityByIdRepository(data.userId);

        // Si existe el perfil no se crea el registro, ya que un usuario no puede tener dos perfiles
        const profileExist = await this.loadProfileByUserId.loadEntityByFieldRepository(data.userId, 'userId');

        if (profileExist) return false

        if (userExist && !profileExist) {
            const profile = await this.addProfileRepository.addEntityRepository({
                ...data, experiences: [], educations: []
            });

            if (profile) return profile;
        }

        return null;
    }
}
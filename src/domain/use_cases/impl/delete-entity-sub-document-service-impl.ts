import {AbstractDeleteSubDocumentServiceImpl} from "./abstract-delete-sub-document-service-impl";
import {ILoadEntityByIdRepository} from "@/domain/models/gateways/load-entity-by-id-repository";
import {IDeleteEntitySubDocumentRepository} from "@/domain/models/gateways/delete-entity-sub-document-repository";

/**
 * Clase que provee el servicio para eliminar un sub-documento en un modelo de una colección
 * @version 1.0
 * @author John Piedrahita
 * @method deleteEntitySubDocumentService Implementación del gateway IDeleteEntitySubDocumentService.
 */
export class DeleteEntitySubDocumentServiceImpl extends AbstractDeleteSubDocumentServiceImpl {
    /**
     * Constructor asigna un valor al gateway genérico para hacer la búsqueda
     * y asigna la data al gateway para insertar el registro
     * @param loadEntityByIdRepository Recibe el valor del id a traves del constructor
     * @param deleteEntitySubDocumentRepository Recibe los parámetros a traves del constructor
     */
    constructor(
        private readonly loadEntityByIdRepository: ILoadEntityByIdRepository<boolean>,
        private readonly deleteEntitySubDocumentRepository: IDeleteEntitySubDocumentRepository
    ) {
        super()
    }

    /**
     * Esta clase actúa de forma genérica para eliminar un sub-documento en una colección,
     * que tenga un array de objetos de acuerdo a la información que llegue en los parámetros
     * desde el controlador[capa infraestructura] por medio de la interfaz genérica
     * {IDeleteEntitySubDocumentService}, elimina el sub-documento en uno, o el otro array
     *
     * @param documentId
     * @param param
     * @param collection
     * @param subDocument
     * @param subDocumentId
     */
    async deleteEntitySubDocumentService(documentId: string, param: string, collection: string,
        subDocument: string, subDocumentId: string): Promise<void | boolean> {

        // Buscamos el documento principal que contiene el array de objetos
        const entityDocument = await this.loadEntityByIdRepository.loadEntityByIdRepository(documentId)

        // Si el documento no existe se retorna null al controlador, para que
        // se lance la excepción
        if (!entityDocument) return null

        // Esta bandera nos ayuda a filtrar y encontrar el objeto en el array(sub-document)
        let entitySubDocument = false

        // Recorremos los objetos que trae el array, si hay una coincidencia la bandera
        // {entitySubDocument} cambia a true, esto significa que existe el objeto
        entityDocument[subDocument].map(item => {
            if (item.id === subDocumentId) entitySubDocument = true
        })

        // En caso de que no exista el objeto en el array(sub-document), se retorna false
        // al controlador para que se lance la excepción.
        if (!entitySubDocument) return false

        // Validamos que exista el documento y el sub-documento para poder eliminarlo
        if (entityDocument && entitySubDocument) {
            await this.deleteEntitySubDocumentRepository.deleteEntitySubDocumentRepository(
                documentId, param, collection, subDocument, subDocumentId
            )
        }
    }
}
import {IDeleteEntitySubDocumentService} from "@/domain/use_cases/gateways/delete-entity-sub-document-service";

/**
 * Clase abstracta que hereda a DeleteEntitySubDocumentServiceImpl
 * @version 1.0
 * @author John Piedrahita
 */
export abstract class AbstractDeleteSubDocumentServiceImpl implements IDeleteEntitySubDocumentService {

    abstract deleteEntitySubDocumentService(documentId: string, param: string, collection: string, subDocument: string, subDocumentId: string): Promise<void | boolean>

}
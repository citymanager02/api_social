import {PostModel} from "@/domain/models/post-model";
import {ILoadEntitiesService} from "@/domain/use_cases/gateways/load-entities-service";
import {ILoadEntitiesRepository} from "@/domain/models/gateways/load-entities-repository";

/**
 * Clase que provee el servicio para buscar PostModel[] en una tabla o colección
 * @version 1.0
 * @author John Piedrahita
 */
export class LoadPostsServiceImpl implements ILoadEntitiesService<PostModel> {
    /**
     * Constructor asigna un valor a los gateway
     * @param loadPostsRepository
     */
    constructor(
        private readonly loadPostsRepository: ILoadEntitiesRepository<PostModel>
    ) {
    }

    /**
     * Esta función retorna un array de posts
     */
    async loadEntitiesService(): Promise<PostModel[]> {
        // Buscamos los post por medio del gateway {ILoadEntitiesRepository}
        const posts = await this.loadPostsRepository.loadEntitiesRepository()

        // Si existen los retornamos, en caso contrario se retorna un []
        if (posts) return posts
    }
}
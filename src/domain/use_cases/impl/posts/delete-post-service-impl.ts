import {IDeletePostService} from "@/domain/use_cases/gateways/posts/delete-post-service";
import {IDeletePostRepository} from "@/domain/models/gateways/posts/delete-post-repository";
import {ILoadEntityByIdRepository} from "@/domain/models/gateways/load-entity-by-id-repository";

/**
 * Clase que provee el servicio para eliminar un PostModel en una colección
 * @version 1.0
 * @author John Piedrahita
 */
export class DeletePostServiceImpl implements IDeletePostService {
    /**
     * Constructor asigna un valor a los gateways genéricos
     * @param deletePostRepository
     * @param loadPostByIdRepository
     */
    constructor(
        private readonly deletePostRepository: IDeletePostRepository,
        private readonly loadPostByIdRepository: ILoadEntityByIdRepository<boolean>
        ) {
    }

    /**
     * Esta función se encarga primero que el post que se va a eliminar exista por
     * medio {id} que llega en el gateway {ILoadEntityByIdRepository}, desde el controlador,
     * si el post existe procede a eliminarlo.
     * @param id
     */
    async deletePostService(id: string): Promise<void> {
        // Buscamos el post que se va a eliminar
        const post = await this.loadPostByIdRepository.loadEntityByIdRepository(id)

        // Si no existe se retorna un null al controlador para que maneje la excepción.
        if (!post) return null

        // Eliminamos el post
        await this.deletePostRepository.deletePostRepository(id)
    }
}
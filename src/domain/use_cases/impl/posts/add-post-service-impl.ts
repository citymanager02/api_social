import {IAddEntityService} from "@/domain/use_cases/gateways/add-entity-service";
import {PostModel} from "@/domain/models/post-model";
import {IAddEntityRepository} from "@/domain/models/gateways/add-entity-repository";
import {ILoadEntityByIdRepository} from "@/domain/models/gateways/load-entity-by-id-repository";
import {UserModel} from "@/domain/models/user-model";

/**
 * Clase que provee el servicio para insertar un PosteModel en una colección
 * @version 1.0
 * @author John Piedrahita
 */
export class AddPostServiceImpl implements IAddEntityService<PostModel> {
    /**
     * Constructor asigna un valor a los gateway genérico.
     * @param addPostRepository
     * @param loadUserByIdRepository
     */
    constructor(
        private readonly addPostRepository: IAddEntityRepository<PostModel>,
        private readonly loadUserByIdRepository: ILoadEntityByIdRepository<UserModel>
    ) {
    }

    /**
     * Esta clase hace la implementación del gateway {IAddEntityService} para crear un
     * {Post}, luego de recibir la {data} del controlador[capa infraestructura], valida que
     * exista el {UserModel} por medio del gateway {ILoadEntityByIdRepository}, para
     * asignarle el post que se va a crear y se comunica por medio del gateway {IAddEntityRepository}
     * con el adaptador de la DB, quien hace la inserción del registro.
     *
     * Nota: Puede ser cualquier gestor de BD, la implementación no depende de la BD,
     * el gestor de BD debe de adaptarse al servicio.
     * "Las dependencias siempre son de afuera hacia adentro"
     * @param data
     */
    async addEntityService(data: PostModel): Promise<boolean | PostModel> {
        // Validamos que el {UserModel} exista, para asignar el {Post} que se va crear
        const userExist = await this.loadUserByIdRepository.loadEntityByIdRepository(data.userId)

        // Si no existe el modelo, retornamos un false al controlador[capa infraestructura]
        // y lance el error correspondiente.
        if (!userExist) return false

        // Si el modelo existe enviamos la data para crear el registro en la DB
        if (userExist) {
            const post = await this.addPostRepository.addEntityRepository({
                    userId : userExist['id'],
                    name: userExist['name'],
                    avatar: userExist['avatar'],
                    ...data,
                    likes: [],
                    comments: []
                },
            )
            // Si el registro se crea de forma correcta, devolvemos el modelo
            if (post) return post
        }
        return null
    }
}
import {ILoadEntityByIdService} from "@/domain/use_cases/gateways/load-entity-by-id-service";

/**
 * Clase abstracta que hereda a LoadEntityDocumentByIdServiceImpl
 * @version 1.0
 * @author John Piedrahita
 */
export abstract class AbstractLoadDocumentByIdServiceImp<T> implements ILoadEntityByIdService<T> {

    abstract loadEntityByIdService(id: string | number): Promise<ILoadEntityByIdService.Result | T>

}
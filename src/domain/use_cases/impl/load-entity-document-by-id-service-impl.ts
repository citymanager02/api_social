import {AbstractLoadDocumentByIdServiceImp} from "./abstract-load-document-by-id-service-impl";
import {ILoadEntityByIdService} from "@/domain/use_cases/gateways/load-entity-by-id-service";
import {ILoadEntityByIdRepository} from "@/domain/models/gateways/load-entity-by-id-repository";

/**
 * Clase que provee el servicio para buscar un modelo por su id en una tabla o colección
 * @version 1.0
 * @author John Piedrahita
 * @method loadEntityByIdService Implementación del gateway ILoadEntityByIdService.
 */
export class LoadEntityDocumentByIdServiceImpl<T> extends AbstractLoadDocumentByIdServiceImp<T>{
    /**
     * Constructor asigna un valor al gateway genérico para hacer la búsqueda
     * de un registro por medio de inyección de dependencias.
     * @param loadEntityByIdRepository
     */
    constructor(
        private readonly loadEntityByIdRepository: ILoadEntityByIdRepository<T>
    ) {
        super();
    }

    /**
     * Esta función nos trae un registro por el {@code id} en la colección o tabla, de
     * acuerdo a la información que llegue en los parámetros desde el controlador
     * [capa infraestructura] por medio de la gateway genérica {@code ILoadEntityByIdRepository},
     * la cual se comunica con el driver adapter[capa infraestructura] para hacer
     * la consulta en la BD.
     * Se maneja dos tipos de datos para el {@code id} en el gateway {@code ILoadEntityByIdService},
     * asi flexibilizamos el gestor de base de datos y creamos las dependencia de
     * afuera hacia adentro. Esto garantiza que el servicio no sepa que gestor de base de
     * datos hace su implementación.
     *
     * @param id
     *
     * @return boolean | T
     */
    async loadEntityByIdService(id: string | number): Promise<ILoadEntityByIdService.Result | T> {
        // Hacemos la búsqueda de que exista el modelo que se va a retornar.
        const entityExist = await this.loadEntityByIdRepository.loadEntityByIdRepository(id)

        // Si el modelo no se encuentra, devolvemos false al controlador y este
        // se encarga de manejar la excepción.
        if (!entityExist) return false

        // Cuando el modelo exista se retorna al controlador
        if (entityExist) return entityExist
    };
}
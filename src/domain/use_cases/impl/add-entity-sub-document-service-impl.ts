import {AbstractAddSubDocumentServiceImpl} from "../impl/abstract-add-sub-document-service-impl";
import {ILoadEntityByIdRepository} from "@/domain/models/gateways/load-entity-by-id-repository";
import {IAddEntitySubDocumentRepository} from "@/domain/models/gateways/add-entity-sub-document-repository";

/**
 * Clase que provee el servicio para insertar un modelo en una colección
 * @version 1.0
 * @author John Piedrahita
 */
export class AddEntitySubDocumentServiceImpl<T> extends AbstractAddSubDocumentServiceImpl<T> {
    /**
     * @param loadEntityByIdRepository
     * @param addEntitySubDocumentRepository
     */
    constructor(
        private readonly loadEntityByIdRepository: ILoadEntityByIdRepository<T>,
        private readonly addEntitySubDocumentRepository: IAddEntitySubDocumentRepository<T>
    ) {
        super()
    }

    /**
     * Esta función actúa de forma genérica y valida que exista el documento principal
     * para agregar un sub-documento en una colección que tenga un array de objetos, de
     * acuerdo a la información que llegue en los parámetros desde el controlador[capa infraestructura]
     * por medio del gateway {IAddEntitySubDocumentRepository}, y asi agregar el
     * sub-documento en el array correspondiente.
     * @param id
     * @param data
     * @param args
     */
    async addEntitySubDocumentService(id: string, data: T, args): Promise<void | boolean> {
        // Buscamos que exista el documento principal para inserta el registro en el sub documento
        const principalEntityExist = await this.loadEntityByIdRepository.loadEntityByIdRepository(id)

        // Si el documento no existe, retornamos al controlador[capa de infraestructura] null,
        // para que se lance la excepción
        if (!principalEntityExist) return null;

        // Esta validación corresponde únicamente cuando un usuario va a dar like a un post,
        // en caso de que lo haya hecho se retorna un false al controlador para que este
        // lance la excepción
        if (principalEntityExist[args[2]].length !== 0 &&
            args[2] === 'likes' && principalEntityExist[args[2]].map(
                item => item.userId === principalEntityExist['userId'])) {
            return false
        }

        // await this.likeExistsInTheDocument(principalEntityExist, args[2])

        // Agregamos el registro al sub-documento
        await this.addEntitySubDocumentRepository.addEntitySubDocumentRepository(
            id, {...data, userId: principalEntityExist['userId']}, args
        )
    }

    async likeExistsInTheDocument(principal, subDocument): Promise<boolean> {
        if (principal[subDocument].length !== 0 &&
            subDocument === 'likes' && principal[subDocument].map(
                item => item.userId === principal['userId'])) {
            return false
        }
    }
}
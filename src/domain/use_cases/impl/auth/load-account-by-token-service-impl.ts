import {ILoadAccountByTokenService} from "../../gateways/auth/load-account-by-token-service";
import {IDecrypt} from "../../helpers/gateways/decrypt";
import {ILoadAccountByTokenRepository} from "../../../models/gateways/auth/load-account-by-token-repository";

/**
 * Clase que provee el servicio para validar el token en la colección de {users}
 * @version 1.0
 * @author John Piedrahita
 */
export class LoadAccountByTokenServiceImpl implements ILoadAccountByTokenService {
    /**
     * Constructor asigna un valor a los gateways
     * @param decrypt
     * @param loadAccountByTokenRepository
     */
    constructor(
        private readonly decrypt: IDecrypt,
        private readonly loadAccountByTokenRepository: ILoadAccountByTokenRepository
    ) {
    }

    /**
     * Esta clase nos hace la búsqueda por el {accessToken} en la colección {users}, de
     * acuerdo a la información que llegue en los parámetros desde el controlador[capa infraestructura]
     * por medio de la interfaz genérica {ILoadAccountByTokenService}
     * @param accessToken
     */
    async loadByTokenService(accessToken: string): Promise<ILoadAccountByTokenService.Result> {
        let token: string

        try {
            // Decodificamos el token que para hacer la validación en la colección
            token = await this.decrypt.decrypt(accessToken)
        } catch (e) {
            return null
        }

        // Si el token coincide con el existente en la base de datos, el usuario
        // puede continuar en el sistema.
        if (token) {
            const account = await this.loadAccountByTokenRepository.loadByTokenRepository(accessToken)
            if (account) return account
        }
        return null
    }
}
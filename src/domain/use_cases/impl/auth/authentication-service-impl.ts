import {IAuthentication} from "../../../models/gateways/auth/authentication-repository";
import {IHashCompare} from "../../helpers/gateways/hash-compare";
import {IUpdateAccessTokenRepository} from "../../../models/gateways/auth/update-access-token-repository";
import {IEncrypt} from "../../helpers/gateways/encrypt";
import {ILoadEntityByFieldRepository} from "@/domain/models/gateways/load-entity-by-field-repository";

/**
 * Clase que provee el servicio para la autenticación en el sistema
 * @version 1.0
 * @author John Piedrahita
 */
export class AuthenticationServiceImpl implements IAuthentication {
    /**
     * Constructor asigna un valor a los gateways
     * @param hashCompare
     * @param encrypt
     * @param loadUserByEmailRepository
     * @param updateAccessTokenRepository
     */
    constructor(
        private readonly hashCompare: IHashCompare,
        private readonly encrypt: IEncrypt,
        private readonly loadUserByEmailRepository: ILoadEntityByFieldRepository<any>,
        private readonly updateAccessTokenRepository: IUpdateAccessTokenRepository
    ) {
    }

    /**
     * Esta clase hace el manejo de la autenticación basado en la colección {users}, de acuerdo
     * a la información que llegue en los parámetros desde el controlador[capa infraestructura]
     * por medio de la interfaz {IAuthentication}
     * @param authenticationParams
     */
    async auth(authenticationParams: IAuthentication.Params): Promise<IAuthentication.Result> {

        // Buscamos que exista la cuenta que se va a autenticar
        const account = await this.loadUserByEmailRepository.loadEntityByFieldRepository(authenticationParams.email, 'email')
        let isValid: boolean

        try {
            // Validamos que las contraseñas sean iguales para generar el token
            isValid = await this.hashCompare.compare(authenticationParams.password, account.password)
        } catch (e) {
            return null
        }

        if (isValid) {
            // El token recibe como argumento  el id de la cuenta, para poder generarlo
            const accessToken = await this.encrypt.encrypt(account.id)
            await this.updateAccessTokenRepository.updateAccessToken(account.id, accessToken)
            // Retornamos el token al cliente
            return {
                accessToken,
                name: account.name
            }
        }
        return null
    }
}
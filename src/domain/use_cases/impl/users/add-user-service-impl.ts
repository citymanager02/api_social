import {UserModel} from "../../../models/user-model";
import {IHash} from "../../helpers/gateways/hash";
import {IAddEntityService} from "@/domain/use_cases/gateways/add-entity-service";
import {IAddEntityRepository} from "@/domain/models/gateways/add-entity-repository";
import {ILoadEntityByFieldRepository} from "@/domain/models/gateways/load-entity-by-field-repository";

/**
 * Clase que provee el servicio para insertar un UserModel en una colección
 * @version 1.0
 * @author John Piedrahita
 */
export class AddUserServiceImpl implements IAddEntityService<UserModel> {
    /**
     *  Constructor asigna un valor a los gateways genéricos.
     * @param checkUserRepository
     * @param addUserRepository
     * @param hashed
     */
    constructor(
        private readonly checkUserRepository: ILoadEntityByFieldRepository<string>,
        private readonly addUserRepository: IAddEntityRepository<UserModel>,
        private readonly hashed: IHash
    ) {
    }

    /**
     * Esta función se encarga de insertar un UserModel, verificando que el email sea único por
     * medio del gateway {ILoadEntityByFieldRepository}, creando un hash para el password con el
     * gateway {IHash} e insertando el UserModel con el gateway genérico {IAddEntityRepository}
     * @param data
     */
    async addEntityService(data: UserModel): Promise<boolean | UserModel> {
        // Verificamos que el email sea único en la BD
        const exist = await this.checkUserRepository.loadEntityByFieldRepository(data.email, 'email')

        if (!exist) {
            // Implementamos la interface IHash para crear un password con mas seguridad
            const hashedPassword = await this.hashed.hash(data.password)

            // Esta implementación luego se comunicara con el driven adapter que tengamos
            // como gestor de base de datos (MongoDB, Postgres, etc..)
            const user = await this.addUserRepository.addEntityRepository({...data, password: hashedPassword})

            // Retornamos el usuario creado a la capa de infraestructura
            if (user) return user
        }
        return null
    }
}

import {IAddEntitySubDocumentService} from "@/domain/use_cases/gateways/add-entity-sub-document-service";

/**
 * Clase abstracta que hereda a AddEntitySubDocumentServiceImpl
 * @version 1.0
 * @author John Piedrahita
 */
export abstract class AbstractAddSubDocumentServiceImpl<T> implements IAddEntitySubDocumentService<T>{
    abstract addEntitySubDocumentService(id: string, data: T, args): Promise<void | boolean>
}
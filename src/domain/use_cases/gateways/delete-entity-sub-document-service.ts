export interface IDeleteEntitySubDocumentService {
    deleteEntitySubDocumentService: (documentId: string, param: string, collection: string,
        subDocument: string, subDocumentId: string) => Promise<void | boolean>
}
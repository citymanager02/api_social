/**
 * Esta interfaz nos provee una abstracción para insertar un objeto en un sub documento.
 * @version 1.0
 * @autor John Piedrahita
 *
 * @param id Identificador del documento principal
 * @param data Objeto que se va a insertar en el sub documento
 * @param ...args Configuración
 *
 * @return void | boolean
 */
export interface IAddEntitySubDocumentService<T> {
    addEntitySubDocumentService: (id: string, data: T, ...args) => Promise<void | boolean>
}
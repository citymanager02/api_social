export interface ILoadEntitiesService<T> {
    loadEntitiesService: () => Promise<T[]>
}
import {PostLikesModel} from "@/domain/models/post-model";

export interface IAddPostLikeService {
    addPostLikeService: (id: string, data?: PostLikesModel) => Promise<IAddPostLikeService.Result | boolean>
}

export namespace IAddPostLikeService {
    export type Result = {
        id: string
        userId: string
    }
}
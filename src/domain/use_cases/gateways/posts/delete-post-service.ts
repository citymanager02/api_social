export interface IDeletePostService {
    deletePostService: (id: string) => Promise<void>
}
export interface ILoadAccountByTokenService {
    loadByTokenService: (accessToken: string) => Promise<ILoadAccountByTokenService.Result>
}

export namespace ILoadAccountByTokenService {
    export type Result = {
        id: string
    }
}
export interface ILoadEntityByFieldService<T, P = any> {
    loadEntityFieldService: (field: T, param?: P) => Promise<void>
}
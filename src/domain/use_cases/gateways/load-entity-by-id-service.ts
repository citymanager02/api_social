export interface ILoadEntityByIdService<T> {
    loadEntityByIdService: (id: string | number) => Promise<T | ILoadEntityByIdService.Result>
}

export namespace ILoadEntityByIdService {
    export type Result = boolean
}
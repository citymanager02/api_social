/**
 * Esta interface nos crear un algoritmo para que el password
 * tenga mas seguridad cuando se registra un usuario.
 *
 * @return string
 */

export interface IHash {
    hash: (text: string) => Promise<string>
}
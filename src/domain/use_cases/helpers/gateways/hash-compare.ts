/**
 * Esta interface hace la comparación de los texto que llegan en
 * el password cuando se registra un usuario.
 *
 * @return Boolean
 */


export interface IHashCompare {
    compare: (text: string, digest: string) => Promise<boolean>
}
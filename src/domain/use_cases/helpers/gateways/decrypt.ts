/**
 * Esta Interface nos hace legible la informacion del usuario(nos devuelve un JSON), para
 * utilizarla en el cliente, ya que se utilizara una libreria externa.
 *
 * @library jsonwebtoken
 * @return string
 */

export interface IDecrypt {
    decrypt: (ciphertext: string) => Promise<string>
}
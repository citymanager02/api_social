/**
 * Esta Interface nos encrypta la informacion del usuario, para
 * devolvernos la data en un token, ya que se utilizara una libreria externa.
 *
 * @library jsonwebtoken
 * @return string
 */

export interface IEncrypt {
    encrypt: (plaintext: string) => Promise<string>
}

/**
 * Interfaz genérica que nos provee un método genérico para buscar un array
 * en una colección o tabla, dependiendo del gestor de base de datos.
 */
export interface ILoadEntitiesRepository<T> {
    loadEntitiesRepository: () => Promise<T[]>
}
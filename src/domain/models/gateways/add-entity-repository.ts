/**
 * Interfaz genérica que nos provee un método genérico para inserta un registro
 * en una colección o tabla, dependiendo del gestor de base de datos.
 */
export interface IAddEntityRepository<T> {
    addEntityRepository: (data: T) => Promise<T>
}
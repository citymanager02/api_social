/**
 * Esta interfaz nos proporciona dos tipo de datos de entrada, esto nos
 * ayuda, para poder ser utilizada en búsquedas por el {_id} en DB NoSQL o
 * {id} en DB SQL, indiferente del motor de que sea utilizado en su implementación.
 */
export interface ILoadEntityByIdRepository<T> {
    loadEntityByIdRepository: (id: number | string) => Promise<T | ILoadEntityByIdRepository.Result>
}

export namespace ILoadEntityByIdRepository {
    export type Result = boolean
}
/**
 * Interfaz genérica que nos provee un método genérico para eliminar un sub documento
 * en una colección, solo aplica para gestores NoSQL
 */
export interface IDeleteEntitySubDocumentRepository {
    deleteEntitySubDocumentRepository: (documentId: string, param: string, collection: string,
        subDocument: string, subDocumentId: string) => Promise<void>
}
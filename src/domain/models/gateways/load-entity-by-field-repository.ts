/**
 * Interfaz genérica que nos provee un método genérico para buscar un elemento
 * en una colección o tabla, dependiendo del gestor de base de datos.
 */
export interface ILoadEntityByFieldRepository<T> {
    loadEntityByFieldRepository: (value: T, param?: string) => Promise<void | boolean | T>
}
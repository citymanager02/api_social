import {PostModel} from "@/domain/models/post-model";
import {ProfileModel} from "@/domain/models/profile-model";
import {UserModel} from "@/domain/models/user-model";

export type AddPostParams = Omit<PostModel, 'id'>
export type AddProfileParams = Omit<ProfileModel, 'id'>
export type AddUserParams = Omit<UserModel, 'id'>
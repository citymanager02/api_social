export interface ILoadAccountByTokenRepository {
    loadByTokenRepository: (token: string) => Promise<ILoadAccountByTokenRepository.Result>
}

export namespace ILoadAccountByTokenRepository {
    export type Result = {
        id: string
    }
}
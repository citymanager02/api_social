export interface IDeleteProfileRepository {
    deleteProfileRepository: (userId: string) => Promise<void>
}
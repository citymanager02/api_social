/**
 * Interfaz genérica que nos provee un método para inserta un sub documento
 * en una colección, solo aplica para gestores NoSQL
 */
export interface IAddEntitySubDocumentRepository<T> {
    addEntitySubDocumentRepository: (id: string, data: T, ...args) => Promise<T | void>
}

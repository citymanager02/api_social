export interface IDeletePostLikeRepository {
    deletePostLikeRepository: (postId: string, likeId: string) => Promise<void>
}
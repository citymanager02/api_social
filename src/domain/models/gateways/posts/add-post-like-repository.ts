import {PostLikesModel} from "@/domain/models/post-model";

export interface IAddPostLikeRepository {
    addPostLikeRepository: (id: string, data?: PostLikesModel) => Promise<IAddPostLikeRepository.Result>
}

export namespace IAddPostLikeRepository {
    export type Result = {
        id: string,
        userId: string
    }
}
export interface IDeletePostRepository {
    deletePostRepository: (id: string) => Promise<void>
}
import {HttpRequest, serverError} from "../../../src/infrastructure/helpers/http";
import * as faker from "faker";
import {LoadUserByIdController} from "../../../src/infrastructure/entry_points/api/users/load-user-by-id-controller";
import {MockLoadUserByIdServiceSpy} from "../../domain/mocks/mock-user-spy";
import {throwError} from "../../domain/mocks/mock-error";
import {ServerError} from "../../../src/infrastructure/helpers/errors";

const mockRequest = (): HttpRequest => {
    return {
        params: faker.random.uuid()
    }
}

type SutTypes = {
    sut: LoadUserByIdController
    mockLoadUserByIdServiceSpy: MockLoadUserByIdServiceSpy
}

const makeSut = (): SutTypes => {
    const mockLoadUserByIdServiceSpy = new MockLoadUserByIdServiceSpy()
    const sut = new LoadUserByIdController(mockLoadUserByIdServiceSpy)

    return { sut, mockLoadUserByIdServiceSpy }
}

describe('Load user Controller', () => {
    it('should call load user with corrects values', async  function () {
        const {sut} = makeSut()
        const request = mockRequest()
        const httpResponse = await sut.handle(request)
        expect(httpResponse).toBeTruthy()
    });

    it('should return 500 if load user throws', async  function () {
        const {sut, mockLoadUserByIdServiceSpy} = makeSut()
        jest.spyOn(mockLoadUserByIdServiceSpy, 'loadEntityByIdService').mockImplementationOnce(throwError)
        const httpResponse = await sut.handle(mockRequest())
        expect(httpResponse).toEqual(serverError(new ServerError()))
    });
})
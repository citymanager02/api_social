import * as faker from 'faker'
import {LoginController} from "../../../src/infrastructure/entry_points/api/auth/login-controller";
import {AuthenticationSpy} from "../../domain/mocks/mock-authentication-spy";
import {HttpRequest, serverError, unauthorized, unprocessableEntity} from "../../../src/infrastructure/helpers/http";
import {throwError} from "../../domain/mocks/mock-error";

const mockRequest = (): HttpRequest => {
    const password = faker.internet.password()
    return {
        body: {
            email: faker.internet.email(),
            password: password,
        }
    }
}

type SutTypes = {
    sut: LoginController
    authenticationSpy: AuthenticationSpy
}

const makeSut = (): SutTypes => {
    const authenticationSpy = new AuthenticationSpy()
    const sut = new LoginController(authenticationSpy)
    return {
        sut,
        authenticationSpy
    }
}

const mockFieldsValidation = (): HttpRequest => {
    return {
        body: {
            email: "",
            password: ""
        },

    }
}

describe('Login controller', () => {
    it('should call authentication with correct values', async  function () {
        const {sut, authenticationSpy} = makeSut()
        const request = mockRequest()
        await sut.handle(request)
        expect(authenticationSpy.params).toEqual({
            email: request.body.email,
            password: request.body.password
        })
    });

    it('should return 401 if invalid credentials are provided', async  function () {
        const {sut, authenticationSpy} = makeSut()
        authenticationSpy.result = null
        const httpResponse = await sut.handle(mockRequest())
        expect(httpResponse).toEqual(unauthorized())
    });

    it('should return 500 if authentication throws', async  function () {
        const {sut, authenticationSpy} = makeSut()
        jest.spyOn(authenticationSpy, 'auth').mockImplementationOnce(throwError)
        const httpResponse = await sut.handle(mockRequest())
        expect(httpResponse).toEqual(serverError(new Error()))
    });

    it('should return 422 if fields this errors', async function () {
        const {sut} = makeSut()
        const httpResponse = await sut.handle(mockFieldsValidation())
        expect(httpResponse).toEqual(unprocessableEntity({
            "email": "email fields is required",
            "password": "password fields is required"
        }))
    });
})
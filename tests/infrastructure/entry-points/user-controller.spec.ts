import {forbidden, HttpRequest, serverError, unprocessableEntity} from "../../../src/infrastructure/helpers/http";
import * as faker from "faker";
import {AddUserController} from "../../../src/infrastructure/entry_points/api/users/add-user-controller";
import {MockUserSpy} from "../../domain/mocks/mock-user-spy";
import {EmailInUseError, ServerError} from "../../../src/infrastructure/helpers/errors";
import {throwError} from "../../domain/mocks/mock-error";

const mockRequest = (): HttpRequest => {
    const password = faker.internet.password()
    return {
        body: {
            name: faker.name.findName(),
            email: faker.internet.email(),
            password: password,
            avatar: faker.internet.avatar(),
            date: Date.now()
        }
    }
}

const mockFieldsValidation = (): HttpRequest => {
    return {
        body: {
            name: "",
            email: "",
            password: "",
            avatar: ""
        },

    }
}

type SutTypes = {
    sut: AddUserController
    addUserSpy: MockUserSpy
}

const makeSut = (): SutTypes => {
    const addUserSpy = new MockUserSpy()
    const sut = new AddUserController(addUserSpy)

    return {sut, addUserSpy}
}

describe('User Controller', () => {
    it('should call add user with corrects values', async function () {
        const {sut} = makeSut()
        const request = mockRequest()
        const user = await sut.handle(request)
        expect(user).toBeTruthy()
    });

    it('should return 403 if user exist by email', async function () {
        const {sut, addUserSpy} = makeSut()
        addUserSpy.userModel = null
        const httpResponse = await sut.handle(mockRequest())
        expect(httpResponse).toEqual(forbidden(new EmailInUseError()))
    });

    it('should return 422 if fields this errors', async function () {
        const {sut} = makeSut()
        const httpResponse = await sut.handle(mockFieldsValidation())
        expect(httpResponse).toEqual(unprocessableEntity({
            "name": "name fields is required",
            "email": "email fields is required",
            "password": "password fields is required",
            "avatar": "avatar fields is required"
        }))
    });

    it('should return 500 if add user throws', async function () {
        const {sut, addUserSpy} = makeSut()
        jest.spyOn(addUserSpy, 'addEntityService').mockImplementationOnce(throwError)
        const httpResponse = await sut.handle(mockRequest())
        await expect(httpResponse).toEqual(serverError(new ServerError()))
    });
})



import {MongoHelper} from "../../../src/infrastructure/driver_adapters/helpers/mongo-helper";
import {Collection} from "mongodb";
import {UserMongoRepositoryAdapter} from "../../../src/infrastructure/driver_adapters/adapters/mongo-adapter/user-mongo-repository-adapter";
import {mockAddUserParams} from "../../domain/mocks/mock-user-params";
import * as faker from "faker";
import dotenv from "dotenv";
import fs from "fs";

if (fs.existsSync(".env")) {
    dotenv.config({ path: ".env" });
} else {
    dotenv.config({ path: ".env.example" });  // you can delete this after you create your own .env file!
}

let userCollection: Collection

const URI = "mongodb+srv://famibienstar:Root2018*@cluster0.qmvza.mongodb.net/api_social_v1?retryWrites=true&w=majority"

describe('User mongo adapter', () => {
    let name = faker.name.findName()
    let email = faker.internet.email()
    let password = faker.internet.password()
    let accessToken = faker.random.uuid()

    beforeAll(async () => {
        await MongoHelper.connect(URI)
    })

    afterAll(async () => {
        await MongoHelper.disconnect()
    })

    beforeEach(async () => {
        name = faker.name.findName()
        email = faker.internet.email()
        password = faker.internet.password()
        accessToken = faker.random.uuid()

        userCollection = await MongoHelper.getCollection('users')
        await userCollection.deleteMany({})
    })

    const makeSut = (): UserMongoRepositoryAdapter => {
        return new UserMongoRepositoryAdapter()
    }

    it('should return an user on success', async function () {
        const sut = makeSut()
        const addUserParams = mockAddUserParams()
        const user = await sut.addEntityRepository(addUserParams)
        expect(user).toBeTruthy()
    });

    it('should return an user when email exist', async function () {
        const sut = makeSut()
        const addUserParams = mockAddUserParams()
        await userCollection.insertOne(addUserParams)
        const user = await sut.loadEntityByFieldRepository(addUserParams.email)
        expect(user).toBeTruthy()
    });

    it('should return null if check by email exist', async function () {
        const sut = makeSut()
        const user = await sut.loadEntityByFieldRepository(faker.internet.email())
        expect(user).toBeNull()
    });

    it('should update the account accessToken on success', async  function () {
        const sut = makeSut()
        const result = await userCollection.insertOne(mockAddUserParams())
        const fakeAccount = result.ops[0]
        expect(fakeAccount.accessToken).toBeFalsy()
        const accessToken = faker.random.uuid()
        await sut.updateAccessToken(fakeAccount._id, accessToken)
        const account = await userCollection.findOne({_id: fakeAccount._id})
        expect(account).toBeTruthy()
        expect(account.accessToken).toBe(accessToken)
    });

    it('should return an account on load by token', async  function () {
        const sut = makeSut()
        await userCollection.insertOne({
            name, email, password, accessToken
        })
        const account = await sut.loadByTokenRepository(accessToken)
        expect(account).toBeTruthy()
        expect(account.id).toBeTruthy()
    });

    it('should return an account load by email', async  function () {
        const sut = makeSut()
        const addUserParams = mockAddUserParams()
        await userCollection.insertOne(addUserParams)
        const account = await sut.loadEntityByFieldRepository(addUserParams.email)
        expect(account).toBeTruthy()
    });

    it('should return an account load by id repository', async  function () {
        const sut = makeSut()
        const addUserParams = mockAddUserParams()
        const result = await userCollection.insertOne(addUserParams)
        const id = result.ops[0]._id
        const account = await sut.loadEntityByIdRepository(id)
        expect(account).toBeTruthy()
    });
})
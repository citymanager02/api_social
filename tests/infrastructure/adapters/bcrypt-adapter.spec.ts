// @ts-ignore
import bcrypt from 'bcrypt'
import {BcryptAdapter} from "../../../src/infrastructure/driver_adapters/helpers/bcrypt-adapter";
import {throwError} from "../../domain/mocks/mock-error";

jest.mock('bcrypt', () => ({
    async hash (): Promise<string> {
        return 'hash'
    },

    async compare (): Promise<boolean> {
        return true
    }
}))

const salt = 12

/**
 * Creamos una instancia de la clase y le pasamos la dependencia
 * para poder encriptar el texto recibido como parametro
 */
const makeSut = (): BcryptAdapter => {
    return new BcryptAdapter(salt)
}

describe('Bcrypt Adapter', () => {
    it('should create the hash correctly',  async function () {
        const sut = makeSut()
        const hashSpy = jest.spyOn(bcrypt, 'hash')
        await sut.hash('value')
        expect(hashSpy).toHaveBeenCalledWith('value', salt)
    });

    it('should throws if bcrypt throws',  async function () {
        const sut = makeSut()
        jest.spyOn(bcrypt, 'hash').mockImplementationOnce(throwError)
        const promise = sut.hash('value')
        await expect(promise).rejects.toThrow()
    });

    it('should call compare with correct values', async function () {
        const sut = makeSut()
        const compareSpy = jest.spyOn(bcrypt, 'compare')
        await sut.compare('value', 'hash')
        expect(compareSpy).toHaveBeenCalledWith('value', 'hash')
    });
})
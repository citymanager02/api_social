import {MongoHelper as sut} from '../../../src/infrastructure/driver_adapters/helpers/mongo-helper'
import dotenv from "dotenv";
import fs from "fs";

if (fs.existsSync(".env")) {
    dotenv.config({ path: ".env" });
} else {
    dotenv.config({ path: ".env.example" });  // you can delete this after you create your own .env file!
}

const URI = "mongodb+srv://famibienstar:Root2018*@cluster0.qmvza.mongodb.net/api_social_v1?retryWrites=true&w=majority"

describe("Mongo helper", () => {
    beforeAll(async () => {
        await sut.connect(URI)
    })

    afterAll(async () => {
        await sut.disconnect()
    })

    it('should reconnect if mongo is down',  async function () {
        let userCollection = await sut.getCollection('users')
        expect(userCollection).toBeTruthy()
        await sut.disconnect()
        userCollection = await sut.getCollection('users')
        expect(userCollection).toBeTruthy()
    });
})
import {UserModel} from "../../../src/domain/models/user-model";
import * as faker from "faker";

export const mockUserModel = (): UserModel => ({
    id: faker.random.uuid(),
    name: faker.name.findName(),
    email: faker.internet.email(),
    password: faker.random.word(),
    avatar: faker.internet.avatar(),
    date: faker.date.future()
})
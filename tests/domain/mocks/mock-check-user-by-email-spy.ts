import {ILoadEntityByFieldRepository} from "@/domain/models/gateways/load-entity-by-field-repository";

export class MockCheckUserByEmailSpy implements ILoadEntityByFieldRepository<any> {
    email: string
    result = false

    async loadEntityByFieldRepository(field: any, param: string): Promise<boolean | any> {
        this.email = field
        return this.result
    }
}
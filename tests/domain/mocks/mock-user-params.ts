import * as faker from "faker";
import {IAuthentication} from "../../../src/domain/models/gateways/auth/authentication-repository";
import {AddUserParams} from "@/domain/models/gateways/entity-types";
import {UserModel} from "@/domain/models/user-model";

export const mockAddUserParams = (): AddUserParams => ({
    name: faker.name.findName(),
    email: faker.internet.email(),
    password: faker.random.word(),
    avatar: faker.internet.avatar(),
    date: faker.date.future()
})

export const mockAuthenticationParams = (): IAuthentication.Params => ({
    email: faker.internet.email(),
    password: faker.internet.password()
})

export const mockUserModel = (): UserModel => ({
    id: faker.random.uuid(),
    name: faker.name.findName(),
    email: faker.internet.email(),
    password: faker.random.word(),
    avatar: faker.internet.avatar(),
    date: faker.date.future()
})
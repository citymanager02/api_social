import {IEncrypt} from "../../../src/domain/use_cases/helpers/gateways/encrypt";
import * as faker from "faker";
import {IHash} from "../../../src/domain/use_cases/helpers/gateways/hash";
import {IHashCompare} from "../../../src/domain/use_cases/helpers/gateways/hash-compare";
import {IDecrypt} from "../../../src/domain/use_cases/helpers/gateways/decrypt";

export class EncryptSpy implements IEncrypt {

    ciphertext = faker.random.uuid()
    plaintext: string

    async encrypt(plaintext: string): Promise<string> {
        this.plaintext = plaintext
        return this.ciphertext
    }
}

export class HashSpy implements IHash {

    digest = faker.random.uuid()
    plaintext: string

    async hash(text: string): Promise<string> {
        this.plaintext = text
        return this.digest
    }
}

export class HashCompareSpy implements IHashCompare {
    plaintext: string
    digest: string
    isValid: boolean

    async compare(text: string, digest: string): Promise<boolean> {
        this.plaintext = text
        this.digest = digest
        return this.isValid
    }
}

export class DecryptSpy implements IDecrypt {
    plaintext = faker.internet.password()
    ciphertext: string

    async decrypt(ciphertext: string): Promise<string> {
        this.ciphertext = ciphertext
        return this.plaintext
    }
}
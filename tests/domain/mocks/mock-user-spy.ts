import {UserModel} from "../../../src/domain/models/user-model";
import {mockUserModel} from "./mock-user-model";
import {IAddEntityService} from "@/domain/use_cases/gateways/add-entity-service";
import {AddUserParams} from "@/domain/models/gateways/entity-types";
import {ILoadEntityByIdService} from "@/domain/use_cases/gateways/load-entity-by-id-service";

export class MockUserSpy implements IAddEntityService<UserModel> {
    params: AddUserParams
    userModel = mockUserModel()

    async addEntityService(data: AddUserParams): Promise<boolean | UserModel> {
        this.params = data
        return this.userModel
    }
}

export class MockLoadUserByIdServiceSpy implements ILoadEntityByIdService<string> {
    id: string
    result = true

    async loadEntityByIdService(id: string): Promise<ILoadEntityByIdService.Result> {
        this.id = id
        return this.result
    }
}
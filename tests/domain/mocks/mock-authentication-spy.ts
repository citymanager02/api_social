import * as faker from "faker";
import {IUpdateAccessTokenRepository} from "../../../src/domain/models/gateways/auth/update-access-token-repository";
import {ILoadAccountByTokenRepository} from "../../../src/domain/models/gateways/auth/load-account-by-token-repository";
import {IAuthentication} from "../../../src/domain/models/gateways/auth/authentication-repository";
import {ILoadEntityByFieldRepository} from "@/domain/models/gateways/load-entity-by-field-repository";

export class LoadUserByEmailRepositorySpy implements ILoadEntityByFieldRepository<any> {
    email: string
    result = {
        id: faker.random.uuid(),
        name: faker.name.findName(),
        password: faker.internet.password()
    }

    async loadEntityByFieldRepository(field: any): Promise<boolean | any> {
        this.email = field
        return this.result
    }
}

export class UpdateAccessTokenRepositorySpy implements IUpdateAccessTokenRepository {
    id: string
    token: string

    async updateAccessToken(id: string, token: string): Promise<void> {
        this.id = id
        this.token = token
    }
}

export class LoadAccountByTokenRepositorySpy implements ILoadAccountByTokenRepository {
    token: string
    result = {
        id: faker.random.uuid()
    }

    async loadByTokenRepository(token: string): Promise<ILoadAccountByTokenRepository.Result> {
        this.token = token
        return this.result
    }
}

export class AuthenticationSpy implements IAuthentication {
    params: IAuthentication.Params
    result = {
        accessToken: faker.random.uuid(),
        name: faker.name.findName()
    }

    async auth(authenticationParams: IAuthentication.Params): Promise<IAuthentication.Result> {
        this.params = authenticationParams
        return this.result
    }
}
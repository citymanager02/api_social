import {UserModel} from "../../../src/domain/models/user-model";
import {mockUserModel} from "./mock-user-model";
import {AddUserParams} from "@/domain/models/gateways/entity-types";
import {IAddEntityRepository} from "@/domain/models/gateways/add-entity-repository";
import {ILoadEntityByIdRepository} from "@/domain/models/gateways/load-entity-by-id-repository";

export class MockAddUserRepositorySpy implements IAddEntityRepository<UserModel> {
    params: AddUserParams
    userModel = mockUserModel()

    async addEntityRepository(data: AddUserParams): Promise<UserModel> {
        this.params = data
        return this.userModel
    }
}

export class MockLoadUserByIdRepositorySpy implements ILoadEntityByIdRepository<any> {
    id: string
    result = true

    async loadEntityByIdRepository(id: string): Promise<ILoadEntityByIdRepository.Result> {
        this.id = id
        return this.result
    }
}
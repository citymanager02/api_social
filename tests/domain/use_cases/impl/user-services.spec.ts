import {AddUserServiceImpl} from "../../../../src/domain/use_cases/impl/users/add-user-service-impl";
import {HashSpy} from "../../mocks/mock-cryptography";
import {MockAddUserRepositorySpy} from "../../mocks/mock-add-user-repository-spy";
import {throwError} from "../../mocks/mock-error";
import {mockAddUserParams, mockUserModel} from "../../mocks/mock-user-params";
import {MockCheckUserByEmailSpy} from "../../mocks/mock-check-user-by-email-spy";

type SutTypes = {
    sut: AddUserServiceImpl
    hashSpy: HashSpy
    addUserRepositorySpy: MockAddUserRepositorySpy
    checkRepositorySpy: MockCheckUserByEmailSpy
}

const makeSut = (): SutTypes => {
    const hashSpy = new HashSpy()
    const addUserRepositorySpy = new MockAddUserRepositorySpy()
    const checkRepositorySpy = new MockCheckUserByEmailSpy()
    const sut = new AddUserServiceImpl(
        checkRepositorySpy,
        addUserRepositorySpy,
        hashSpy
    )

    return {
        sut,
        hashSpy,
        addUserRepositorySpy,
        checkRepositorySpy
    }
}

describe('add user use case', () => {
    it('should throw if add user repository throws', async function () {
        const { sut, addUserRepositorySpy } = makeSut()
        jest.spyOn(addUserRepositorySpy, 'addEntityRepository').mockImplementationOnce(throwError)
        const promise = sut.addEntityService(mockUserModel())
        await expect(promise).rejects.toThrow()
    });

    it('should call add user repository with correct values', async function () {
        const { sut, addUserRepositorySpy, hashSpy } = makeSut()
        const addUserParams = mockUserModel()
        await sut.addEntityService(addUserParams)
        expect(addUserRepositorySpy.params).toEqual({
            id: addUserParams.id,
            name: addUserParams.name,
            email: addUserParams.email,
            date: addUserParams.date,
            avatar: addUserParams.avatar,
            password: hashSpy.digest
        })
    });

    it('should return null if check user by email repository exist', async  function () {
        const { sut, checkRepositorySpy } = makeSut()
        checkRepositorySpy.result = true
        const exist = await sut.addEntityService(mockUserModel())
        expect(exist).toBeNull()
    });

    it('should call load user by email repository with correct email', async function () {
        const { sut, checkRepositorySpy } = makeSut()
        const addUserParams = mockUserModel()
        await sut.addEntityService(addUserParams)
        expect(checkRepositorySpy.email).toBe(addUserParams.email)
    });
})
import * as faker from 'faker'
import {LoadAccountByTokenServiceImpl} from "../../../../src/domain/use_cases/impl/auth/load-account-by-token-service-impl";
import {DecryptSpy} from "../../mocks/mock-cryptography";
import {LoadAccountByTokenRepositorySpy} from "../../mocks/mock-authentication-spy";
import {throwError} from "../../mocks/mock-error";

type SutTypes = {
    sut: LoadAccountByTokenServiceImpl,
    decryptSpy: DecryptSpy,
    loadAccountByTokenRepositorySpy: LoadAccountByTokenRepositorySpy
}

const makeSut = (): SutTypes => {
   const  decryptSpy = new DecryptSpy()
    const loadAccountByTokenRepositorySpy = new LoadAccountByTokenRepositorySpy()
    const sut = new LoadAccountByTokenServiceImpl(
        decryptSpy,
        loadAccountByTokenRepositorySpy

    )
    return {
       sut,
        decryptSpy,
        loadAccountByTokenRepositorySpy
    }
}

let token: string

describe('Load account by token use case', () => {
    beforeEach(() => {
        token = faker.random.uuid()
    })

    it('should call decrypt with correct ciphertext', async function () {
        const {sut, decryptSpy} = makeSut()
        await sut.loadByTokenService(token)
        expect(decryptSpy.ciphertext).toBe(token)
    });

    it('should return null if decrypt returns null', async  function () {
        const {sut, decryptSpy} = makeSut()
        decryptSpy.plaintext = null
        const account = await sut.loadByTokenService(token)
        expect(account).toBeNull()
    });

    it('should call load account by token repository with correct values', async function () {
        const {sut, loadAccountByTokenRepositorySpy} = makeSut()
        await sut.loadByTokenService(token)
        expect(loadAccountByTokenRepositorySpy.token).toBe(token)
    });

    it('should return account on success', async function () {
        const {sut, loadAccountByTokenRepositorySpy} = makeSut()
        const account = await sut.loadByTokenService(token)
        await expect(account).toBe(loadAccountByTokenRepositorySpy.result)
    });

    it('should throw if decrypt throws', async function () {
        const {sut, decryptSpy} = makeSut()
        jest.spyOn(decryptSpy, 'decrypt').mockImplementationOnce(throwError)
        const account = await sut.loadByTokenService(token)
        await expect(account).toBeNull()
    });

    it('should throw if load account by token repository throws', async function () {
        const {sut, loadAccountByTokenRepositorySpy} = makeSut()
        jest.spyOn(loadAccountByTokenRepositorySpy, 'loadByTokenRepository').mockImplementationOnce(throwError)
        const promise = sut.loadByTokenService(token)
        await expect(promise).rejects.toThrow()
    });
})
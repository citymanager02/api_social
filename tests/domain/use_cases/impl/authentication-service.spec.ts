import {AuthenticationServiceImpl} from "../../../../src/domain/use_cases/impl/auth/authentication-service-impl";
import {LoadUserByEmailRepositorySpy, UpdateAccessTokenRepositorySpy} from "../../mocks/mock-authentication-spy";
import {EncryptSpy, HashCompareSpy} from "../../mocks/mock-cryptography";
import {mockAuthenticationParams} from "../../mocks/mock-user-params";
import {throwError} from "../../mocks/mock-error";

type SutTypes = {
    sut: AuthenticationServiceImpl
    hashCompareSpy: HashCompareSpy
    encryptSpy: EncryptSpy
    loadUserByEmailRepositorySpy: LoadUserByEmailRepositorySpy
    updateAccessTokenRepositorySpy: UpdateAccessTokenRepositorySpy
}

const makeSut = (): SutTypes => {
    const hashCompareSpy = new HashCompareSpy()
    const encryptSpy = new EncryptSpy()
    const loadUserByEmailRepositorySpy = new LoadUserByEmailRepositorySpy()
    const updateAccessTokenRepositorySpy = new UpdateAccessTokenRepositorySpy()
    const sut = new AuthenticationServiceImpl(
        hashCompareSpy,
        encryptSpy,
        loadUserByEmailRepositorySpy,
        updateAccessTokenRepositorySpy
    )

    return {
        sut,
        hashCompareSpy,
        encryptSpy,
        loadUserByEmailRepositorySpy,
        updateAccessTokenRepositorySpy
    }
}

describe('Authentication use case', () => {
    it('should call load user by email repository with correct email', async function () {
        const {sut, loadUserByEmailRepositorySpy} = makeSut()
        const authenticationParams = mockAuthenticationParams()
        await sut.auth(authenticationParams)
        expect(loadUserByEmailRepositorySpy.email).toBe(authenticationParams.email)
    });

    it('should throw if load user by email repository throws', async function () {
        const {sut, loadUserByEmailRepositorySpy} = makeSut()
        jest.spyOn(loadUserByEmailRepositorySpy, 'loadEntityByFieldRepository').mockImplementationOnce(throwError)
        const promise = sut.auth(mockAuthenticationParams())
        await expect(promise).rejects.toThrow()
    });

    it('should return null if load user by email repository return null', async function () {
        const {sut, loadUserByEmailRepositorySpy} = makeSut()
        loadUserByEmailRepositorySpy.result = null
        const model = await sut.auth(mockAuthenticationParams())
        expect(model).toBeNull()
    });

    it('should return null if hash compare return false', async function () {
        const {sut, hashCompareSpy} = makeSut()
        hashCompareSpy.isValid = false
        const model = await sut.auth(mockAuthenticationParams())
        expect(model).toBeFalsy()
    });

    it('should call hash compare with correct value', async function () {
        const {sut, hashCompareSpy, loadUserByEmailRepositorySpy} = makeSut()
        const authenticationParams = mockAuthenticationParams()
        await sut.auth(authenticationParams)
        expect(hashCompareSpy.plaintext).toBe(authenticationParams.password)
        expect(hashCompareSpy.digest).toBe(loadUserByEmailRepositorySpy.result.password)
    });
})
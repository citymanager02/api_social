import {MockLoadUserByIdRepositorySpy} from "../../mocks/mock-add-user-repository-spy";
import * as faker from "faker";
import {LoadEntityDocumentByIdServiceImpl} from "../../../../src/domain/use_cases/impl/load-entity-document-by-id-service-impl";

type SutTypes = {
    sut: LoadEntityDocumentByIdServiceImpl<any>
    loadUserByIdRepositorySpy: MockLoadUserByIdRepositorySpy
}

const makeSut = (): SutTypes => {
    const loadUserByIdRepositorySpy = new MockLoadUserByIdRepositorySpy()
    const sut = new LoadEntityDocumentByIdServiceImpl(loadUserByIdRepositorySpy)

    return {
        sut,
        loadUserByIdRepositorySpy
    }
}

describe('Load user by id service', () => {
    it('should call load user by id repository with correct values', async  function () {
        const {sut} = makeSut()
        const userExist = await sut.loadEntityByIdService(faker.random.uuid())
        expect(userExist).toBeTruthy()
    });
})